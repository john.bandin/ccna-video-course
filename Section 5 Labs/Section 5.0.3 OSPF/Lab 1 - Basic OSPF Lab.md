# CCNA Lab: Basic OSPF Configuration on Cisco iOS

![](/images/Screenshot%202024-01-19%20at%204.40.05%20PM.png)

## Objective 
In this lab, you will configure OSPF routing between R1 and R2. 

- Configure OSPF routing
- Verify OSPF on both 
- Test reachability

## Lab Setup

1. You will need two Cisco iOS switches and two Cisco iOS Routers. These can be Physical devices or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: LANs on R1 and R2.

At this point in your studies you should be able to configure VLANs, trunks, and Router-on-a-stick for the LANs hanging off R1 and R2. To configure the respective LANs use the configuration below. 


```
!
hostname R1
!
ip domain-name trepatech.com
!
username cisco priv 15 secret cisco
!
crypto key generate rsa modulus 2048
!
line vty 0 4
 transport input ssh
 login local
 exit
!
int gi0/1
 description //ROAS LINK\\
 no shut
 exit
!
int gi0/1.10
 description //VLAN 10\\
 encapsulation dot1q 10
 ip address 172.16.10.1 255.255.255.0
 exit
!
int gi0/1.20 
 description //VLAN 20\\
 encapsulation dot1q 20
 ip address 172.16.20.1 255.255.255.0
 exit
!
int gi0/0
 description //P2P Link\\
 ip address 10.10.10.0 255.255.255.254
 no shut
 exit
!
do wr
!
```

**SW1**

```
hostname SW1
!
int gi0/1 
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport non 
 exit
!
int gi1/0
 switchport mode access
 switchport access vlan 10
 spanning-tree portfast
 exit
!
int gi1/1
 switchport mode access
 switchport access vlan 20
 spanning-tree portfast
 exit
!
ip domain-name trepatech.com
!
username cisco priv 15 secret cisco
!
crypto key generate rsa modulus 2048
!
line vty 0 4
 transport input ssh
 login local
 exit
!
do wr
!
```
**PC5**

```
ip 172.16.10.2/24 172.16.10.1

save
```

**PC6**

```
ip 172.16.20.2/24 172.16.20.1

save
```

**R2**

```
!
hostname R2
!
ip domain-name trepatech.com
!
username cisco priv 15 secret cisco
!
crypto key generate rsa modulus 2048
!
line vty 0 4
 transport input ssh
 login local
 exit
!
int gi0/1
 description //ROAS LINK\\
 no shut
 exit
!
int gi0/1.10
 description //VLAN 10\\
 encapsulation dot1q 10
 ip address 192.168.10.1 255.255.255.0
 exit
!
int gi0/1.20 
 description //VLAN 20\\
 encapsulation dot1q 20
 ip address 192.168.20.1 255.255.255.0
 exit
!
int gi0/0
 description //P2P Link\\
 ip address 10.10.10.1 255.255.255.254
 no shut
 exit
!
do wr
!
```

**SW2**

```
hostname SW2
!
int gi0/1 
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport non 
 exit
!
int gi1/0
 switchport mode access
 switchport access vlan 10
 spanning-tree portfast
 exit
!
int gi1/1
 switchport mode access
 switchport access vlan 20
 spanning-tree portfast
 exit
!
ip domain-name trepatech.com
!
username cisco priv 15 secret cisco
!
crypto key generate rsa modulus 2048
!
line vty 0 4
 transport input ssh
 login local
 exit
!
do wr
!
```

**PC7**

```
ip 192.168.10.2/24 192.168.10.1

save
```

**PC8**

```
ip 192.168.20.2/24 192.168.20.1

save
```

### Step 3: Configure OSPF

Now we will do our OSPF configuration on R1 and R2. We will just do a standard OSPF configuration and only advertise our known networks on both R1 and R2.

**R1**

![](/images/Screenshot%202024-01-19%20at%2011.39.17%20AM.png)

```
router ospf 1
 router-id 1.1.1.1
 network 10.10.10.0 0.0.0.1 area 0
 network 172.16.10.0 0.0.0.255 area 0
 network 172.16.20.0 0.0.0.255 area 0
 exit
!
do wr
!
```

**R2**

![](/images/Screenshot%202024-01-19%20at%2011.39.43%20AM.png)

```
router ospf 1
 router-id 2.2.2.2
 network 10.10.10.0 0.0.0.1 area 0
 network 192.168.10.0 0.0.0.255 area 0
 network 192.168.20.0 0.0.0.255 area 0
 exit
!
do wr
!
```

### Step 4: Verify OSPF routes

Now that we have configured OSPF let's do some verification. Execute these commands below and take note of their output.

![](/images/Screenshot%202024-01-19%20at%2011.40.40%20AM.png)

```
show ip ospf neighbor
```

![](/images/Screenshot%202024-01-19%20at%2011.41.06%20AM.png)

```
show ip ospf database
```

![](/images/Screenshot%202024-01-19%20at%2011.41.37%20AM.png)

```
show ip protocols
```

![](/images/Screenshot%202024-01-19%20at%2011.42.10%20AM.png)

```
show ip route ospf
```

![](/images/Screenshot%202024-01-19%20at%2011.42.34%20AM.png)

```
show ip route
```

### Step 5: Test reachability.

Now we will ping from PC5 to PC8 to test reachability.

![](/images/Screenshot%202024-01-19%20at%202.39.01%20PM.png)

