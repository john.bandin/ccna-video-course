# CCNA Lab: Basic HSRP Configuration on Cisco iOS

![](/images/Screenshot%202024-12-05%20at%208.25.00%E2%80%AFPM.png)

## Objective 
In this lab, you will configure OSPF routing between R1 and R2. 

- Configure HSRP on R1 and R2
- Verify the Active and Standby router
- Test a failover and reachability

## Lab Setup

1. You will need two Cisco iOS Routers and one Cisco iOS Switch. These can be Physical devices or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: R1, R2, SW1 Configuration

First we will set the baseline configuration on R1, R2 and SW1. Paste this configuration below for all the network devices and ensure you change the necessary commands.

```
!
hostname XX
!
ip domain-name trepatech.com
!
username cisco priv 15 secret cisco
!
crypto key generate rsa modulus 2048
!
line vty 0 4
 transport input ssh
 login local
 exit
!
int gi0/0
 description //Default-Gateway\\
 ip address 192.168.1.X 255.255.255.0
 no shut
 exit
!
do wr
!
```

### Step 3: Configure HSRP on R1 and R2


We will configure HSRPv2 and we will make R1 the Active Router by changing the default priority to 110, and we will also configure the `preempt` command to ensure that R1 is always the "active" router.


**R1**
```
conf t
!
interface gi0/0
 standby version 2
 standby 1 ip 192.168.1.1
 standby 1 priority 101
 standby 1 preempt
```

**R2**
```
conf t
!
interface gi0/0
 standby version 2
 standby 1 ip 192.168.1.1
```

### Step 4 Verify HSRP

Now we will verify that R1 is the active and that R2 is the standby. We will also conduct failover by doing a continuous ping from PC1 to the Default Gateway (192.168.1.1), and shutting down R1's Gi0/0 interface. Failover will be a success if we only see 1-2 missed pings, followed successful pings.



```
show standby brief
```
- This command will give us a brief output of HSRP active and standby devices. 

![](/images/Screenshot%202024-12-08%20at%204.48.54%E2%80%AFPM.png)


Now we will go into PC1, set the IP configuration and do a continuous ping to 192.168.1.1

![](/images/Screenshot%202024-12-08%20at%204.51.02%E2%80%AFPM.png)

Now we will shutdown Gi0/0 on R1 and check our pings.

**R1**
```
conf t
!
interface gi0/0
 shut
```

![](/images/Screenshot%202024-12-08%20at%204.55.00%E2%80%AFPM.png)

As we can see the failover worked. Now lets hop into R2 and verify that it became the active router.

![](/images/Screenshot%202024-12-08%20at%204.56.29%E2%80%AFPM.png)

As we can from the output above R2 is now the active. Since we configured the `standby 1 preempt` command on R1, after we turn on R1's Gi0/0 interface, R1 should take over as the Active router. Go into R1 and `no shut` gi0/0, and then verify if it became the Active Router again.

**R1**
```
conf t 
!
interface gi0/0
 no shut
 ```

Now verify with the `show standby brief` command.

```
show standby brief
```

![](/images/Screenshot%202024-12-08%20at%204.58.46%E2%80%AFPM.png)

## Conclusion

In this lab we configured HSRP and conducted a failover test of our HSRP group.

## Summary of HSRP (Hot Standby Router Protocol)

**Hot Standby Router Protocol (HSRP)** is a Cisco proprietary protocol that provides **redundancy for IP networks** by allowing multiple routers to work together to present the illusion of a single virtual router to the devices on the network. This ensures that if the primary router fails, a backup router can seamlessly take over, minimizing downtime and maintaining network availability.

---

## Key Points about HSRP:

1. **Purpose**: HSRP ensures high availability and fault tolerance in a network by providing an automatic failover mechanism for gateways.
2. **Virtual IP Address**: Devices on the network configure the virtual IP address (VIP) of the HSRP group as their default gateway. The actual traffic is routed through the active router in the group.
3. **Active and Standby Roles**:
   - **Active Router**: Handles all traffic directed to the virtual IP address.
   - **Standby Router**: Monitors the active router and takes over if the active router becomes unavailable.
4. **Preemption**: Allows a higher-priority router to take over as the active router when it becomes available again.
5. **HSRP States**: Routers transition through multiple states, such as **Initial**, **Listen**, **Speak**, **Standby**, and **Active**, to determine their roles and ensure smooth failover.
6. **Prioritization**: Priority values can be assigned to routers to dictate which should become the active router; higher values indicate higher priority.

---

## Importance of HSRP:

- **Minimizes Downtime**: Provides near-instantaneous failover to keep the network operational.
- **Simplifies Network Design**: Devices use a single default gateway, reducing configuration complexity.
- **Supports Business Continuity**: Ensures critical applications and services remain accessible during router failures.
- **Enhances User Experience**: Eliminates manual intervention and reduces perceived downtime for end users.

---

## Use Cases:
HSRP is commonly deployed in enterprise networks, data centers, and critical network environments where high availability is essential, such as financial institutions, healthcare systems, and e-commerce platforms.
