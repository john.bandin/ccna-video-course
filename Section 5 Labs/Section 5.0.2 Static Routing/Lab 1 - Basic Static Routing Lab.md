# CCNA Lab: Basic Static Lab Configuration on Cisco iOS

![](/images/Screenshot%202024-01-18%20at%202.26.12%20PM.png)

## Objective 
In this lab, you will configure static routing between two networks. From R1 ---> R2 you will configure two static routes to reach R2's LAN. From R2 ---> R1 you will configure a default route to reach R1's LANs.

- Configure a static route
- Configure a default static route

## Lab Setup
1. You will need two Cisco iOS switches and two Cisco iOS Routers. These can be Physical devices or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)
### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: Configure LANs on R1 and R2

At this point in your studies you should know how to configure ROAS, Trunks, and VLANs. But incase you do not use these baselines below to configure the LANs on R1 and R2

**R1**

```
!
hostname R1
!
ip domain-name trepatech.com
!
username cisco priv 15 secret cisco
!
crypto key generate rsa modulus 2048
!
line vty 0 4
 transport input ssh
 login local
 exit
!
int gi0/1
 description //ROAS LINK\\
 no shut
 exit
!
int gi0/1.10
 description //VLAN 10\\
 encapsulation dot1q 10
 ip address 192.168.10.1 255.255.255.0
 exit
!
int gi0/1.20 
 description //VLAN 20\\
 encapsulation dot1q 20
 ip address 192.168.20.1 255.255.255.0
 exit
!
do wr
!
```

**SW1**

```
hostname SW1
!
int gi0/1 
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport non 
 exit
!
int gi1/0
 switchport mode access
 switchport access vlan 10
 spanning-tree portfast
 exit
!
int gi1/1
 switchport mode access
 switchport access vlan 20
 spanning-tree portfast
 exit
!
ip domain-name trepatech.com
!
username cisco priv 15 secret cisco
!
crypto key generate rsa modulus 2048
!
line vty 0 4
 transport input ssh
 login local
 exit
!
do wr
!
```

**PC1**

```
ip 192.168.10.2/24 192.168.10.1

save
```

**PC2**

```
ip 192.168.20.2/24 192.168.20.1

save
```

**R2**

```
!
hostname R2
!
ip domain-name trepatech.com
!
username cisco priv 15 secret cisco
!
crypto key generate rsa modulus 2048
!
line vty 0 4
 transport input ssh
 login local
 exit
!
int gi0/1
 description //ROAS LINK\\
 no shut
 exit
!
int gi0/1.10
 description //VLAN 10\\
 encapsulation dot1q 10
 ip address 172.16.10.1 255.255.255.0
 exit
!
int gi0/1.20 
 description //VLAN 20\\
 encapsulation dot1q 20
 ip address 172.16.20.1 255.255.255.0
 exit
!
do wr
!
```

**SW2**

```
hostname SW2
!
int gi0/1 
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport non 
 exit
!
int gi1/0
 switchport mode access
 switchport access vlan 10
 spanning-tree portfast
 exit
!
int gi1/1
 switchport mode access
 switchport access vlan 20
 spanning-tree portfast
 exit
!
ip domain-name trepatech.com
!
username cisco priv 15 secret cisco
!
crypto key generate rsa modulus 2048
!
line vty 0 4
 transport input ssh
 login local
 exit
!
do wr
!
```

**PC3**

```
ip 172.16.10.2/24 172.16.10.1

save
```

**PC4**

```
ip 172.16.20.2/24 172.16.20.1

save
```

### Step 3: Configure Point-to-Point

Now we need to configure the IP addressing between R1 and R2. This is not a new command, but its worth noting the subnet we will be using. A `/31`, this is appropriate for P2P links between routing devices.

**R1**

![](/images/Screenshot%202024-01-18%20at%201.56.08%20PM.png)

```
int gi0/0
 description ///P2P -- R2\\\
 ip address 75.0.0.0 255.255.255.254
 no shut
 exit
!
do wr
!
```

**R2**
 
![](/images/Screenshot%202024-01-18%20at%201.57.31%20PM.png)

```
int gi0/0
 description ///P2P -- R1\\\
 ip address 75.0.0.1 255.255.255.254
 no shut
 exit
!
do wr
!
```

### Step 4: Configure static routing R1 --> R2 and verify

Now we will configure two static routes. We need the users (192.168.10.0/24 & 192.168.20.0/24) that sit on R1's LAN to have reachabiltiy to the users(172.16.10.0/24 & 172.16.20.0/24) that sit on R2's LAN. To accomplish this we will configure two static routes that tell R1 where to send traffic to reach the VLAN 10 and VLAN 20 subnets, that sit on R2. 

Use the commands below to set the static route, in these examples one static route will use the exit interface as the destination, and the other will be fully recursive, by using the exit interface and the destination IP address on R2.

![](/images/Screenshot%202024-01-18%20at%202.01.52%20PM.png)

```
!
ip route 172.16.10.0 255.255.255.0 gi0/0
ip route 172.16.20.0 255.255.255.0 gi0/0 75.0.0.1
!
do wr
!
```

Now lets verify that the static routes are in our routing table. We will use two commands to verify this. See commands below and note the difference.

![](/images/Screenshot%202024-01-18%20at%202.04.17%20PM.png)

```
show ip route static
```
- Note this command will just show us the static routes in the routing table.

![](/images/Screenshot%202024-01-18%20at%202.04.40%20PM.png)

```
show ip route
```
- Note this command will show us the entire routing table.

### Step 5: Configure a default route from R2 --> R1 and verify

Now for communication from R2 to R1 we will use a default route. After we configure this default route the users on R1 and R2 should have full reachability between each other.

![](/images/Screenshot%202024-01-18%20at%202.06.18%20PM.png)

```
ip route 0.0.0.0 0.0.0.0 gi0/0
```

Now lets verify that the static routes are in our routing table. We will use two commands to verify this. See commands below and note the difference.

![](/images/Screenshot%202024-01-18%20at%202.06.49%20PM.png)

```
show ip route static
```
- Note this command will just show us the static routes in the routing table.

![](/images/Screenshot%202024-01-18%20at%202.06.58%20PM.png)

```
show ip route
```
- Note this command will show us the entire routing table.

### Step 6: Verify reachability

Now we will conduct two pings tests, one from R1 and the other from PC1 to PC4. 

- From R1 we will ping the default gateway of R2's VLAN 10 Subnet. We will ensure that our pings our sourced correctly
- From PC1 we will ping PC4 to test user end-to-end communication.

From R1 we will ping 172.16.10.1 without sourcing the ping, and then we will ping sourcing from gi0/0.20. We will take a wireshark capture and note the differenc.

**R1**

![](/images/Screenshot%202024-01-18%20at%202.10.42%20PM.png)

```
ping 172.16.10.1
```

![](/images/Screenshot%202024-01-18%20at%202.11.21%20PM.png)

Notice in this packet capture the source IP address is actually the inteface closest to the destination. So this ping test can not be used to defintely say that the users on R1 can reach the users on R2. This is because the source IP address in this ping test is `75.0.0.0`, the routers Gi0/0 interface.

![](/images/Screenshot%202024-01-18%20at%202.13.26%20PM.png)

```
ping 172.16.10.1 source gi0/1.20
```

![](/images/Screenshot%202024-01-18%20at%202.13.50%20PM.png)

This is an accurate way to test reachability because now the ping is coming from the users default gateway. 

Now to fully test reachability let's ping from PC1 to PC4

![](/images/Screenshot%202024-01-18%20at%202.14.44%20PM.png)

# Static Routing in Cisco IOS: An In-Depth Summary

Static routing is a form of routing that occurs when a router uses a manually-configured routing entry, rather than information from dynamic routing traffic. In Cisco IOS (Internetwork Operating System), static routes are used to determine the path that network traffic should take to reach a particular destination. This method is preferred for its simplicity, predictability, and control over routing paths.

## Basics of Static Routing
- **Manual Configuration:** Static routes are manually configured and specify a fixed path to a network destination.
- **Deterministic:** The path of the network traffic is predictable, as it does not change unless manually reconfigured.

## Configuring Static Routes in Cisco IOS
- **Command:** The basic command to configure a static route is `ip route [destination_network] [mask] [next_hop_address or exit_interface]`.
- **Destination Network:** The network or subnet that you want to reach.
- **Mask:** Subnet mask that defines the size of the destination network.
- **Next Hop Address/Exit Interface:** The IP address of the next hop router or the exit interface that leads to the destination network.

## Operational Flow
- **Routing Decision:** When a router receives a packet, it checks its routing table. If a static route for the packet's destination exists, the router forwards the packet to the next hop or exit interface specified by the static route.
- **Directly Connected Networks:** If the destination network is directly connected, the router can forward packets to the destination without needing a next-hop IP address.

## Advantages
- **Simplicity and Stability:** Static routes, being manually configured, provide a simple and stable routing scenario.
- **Resource Efficiency:** They consume less router CPU and memory resources than dynamic routing protocols, as there are no periodic routing updates.
- **Security:** Static routes can be more secure because they do not advertise network information over the network.

## Considerations
- **Scalability:** Not suitable for large networks as it becomes complex and cumbersome to manually maintain accurate routing information.
- **Management Overhead:** Any change in the network topology requires manual reconfiguration of the static routes.
- **No Fault Tolerance:** Static routes do not automatically adapt to network changes, potentially leading to suboptimal routing or black holes in the event of a network failure.

## Use Cases
- **Small Networks:** Ideal for small, stable networks where the network topology does not change frequently.
- **Default Route:** Often used to set a default route (0.0.0.0/0) to an ISP router, directing all unknown traffic towards the ISP.
- **Stub Networks:** Useful in stub networks (networks with a single exit point), where there is only one path to external networks.

In summary, static routing in Cisco IOS provides a straightforward, stable, and resource-efficient routing mechanism, ideal for smaller or more secure network environments. However, careful consideration must be given to the network size, management overhead, and the need for fault tolerance when opting for static over dynamic routing.
