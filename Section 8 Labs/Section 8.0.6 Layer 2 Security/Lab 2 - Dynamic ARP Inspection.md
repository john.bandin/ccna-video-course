# CCNA Lab: Extended and Named ACLs


## Objective

In this lab, you will learn how to configure and apply DHCP Snooping in a campus network. Not in this lab are the configurations for all the VLANs, trunks, sub-interfaces, and DHCP pool on the Cisco Router.

- Determine trusted and untrusted DHCP Snooping interfaces
- Create and understand the difference in the named ACL
- How to edit a named ACL
- The best practice for placing an extended ACL

## Lab Setup

1. You will need two Cisco routers, two Cisco switches, and four endpoints.
2. Access to all devices via console.
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Instructions