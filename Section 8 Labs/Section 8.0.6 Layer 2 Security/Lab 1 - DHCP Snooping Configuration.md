# CCNA Lab: DHCP Snooping Lab


## Objective

In this lab, you will learn how to configure and apply DHCP Snooping in a campus network. Not in this lab are the configurations for all the VLANs, trunks, sub-interfaces, and DHCP pool on the Cisco Router.

- Determine trusted and untrusted DHCP Snooping interfaces
- Configure DHCP Snooping per VLAN
- Verify DHCP Snooping Bindings

## Lab Setup

1. You will need one Cisco router, three Cisco switches, and four endpoints.
2. Access to all devices via console.
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

![](/images/Screenshot%202023-11-02%20at%209.46.08%20PM.png)

## Instructions

### Step 1: Determine which interfaces are trusted and untrusted

If we refer to the diagram above interface `gi0/2` on SW2 and interface `gi0/0` on SW3 are going to be our trusted ports. The rest of the ports can remain the default `untrusted`

### Step 2: Test DHCP functionality

Next lets make sure DHCP is functioning properly before we configure DHCP snooping. Console into any PC and type in `ip dhcp` on the PC's CLI. If DHCP is working correctly then the computer should receive an IP address.

### Step 3: Perform wireshark

Before we start to configure `DHCP Snooping` lets perform two packet captures. Start a wireshark capture on the interface connected to VPCS7 and the interface connecting SW2-->SW3. This way we can see the process of DHCP at the wire.

### Step 4: COnfigure DHCP Snooping

Now we will configure DHCP snooping on SW2 and SW3.

```
Switch#conf t
Switch(config)#ip dhcp snooping
Switch(config)#ip dhcp snooping vlan 10,20
```

This is the basic DHCP snooping configuration. Now let's see if VPCS7 will be able to get an IP address via DHCP. Console into VPCS7 and type in `ip dhcp` on the CLI. 


As we can see the PC was not able to receive an IP address. This is because of something called `option 82`. Option 82 will be automatically added on when the DHCP Discover message gets forwarded from the first switch to the next switch. The untrusted interface will receive that DHCP packet with option 82 and will drop it. So to enable DHCP across multiple switches while using `DHCP Snooping` we will have to disable `option 82`.

### Step 5: Disable option 82

Now lets disable DHCP Snooping adding `option 82` on our DHCP packets.

```
Switch(config)#no ip dhcp snooping information option
```

### Step 6: Verify DHCP 

Now go back to `VPCS7` and type in `ip dhcp` and now our PC will receive an IP address.

### Step 7: Verification

Now lets go to SW2 and check our DHCP Snooping binding table. Type the command below from privilege mode.

```
show ip dhcp snooping binding 
```

