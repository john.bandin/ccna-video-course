# CCNA Lab: Securing Cisco IOS

![](/images/Screenshot%202024-12-09%20at%203.55.08%E2%80%AFPM.png)

## Objective
In this lab, you will learn how to secure access to a Cisco IOS device. By the end of this lab, you should be able to:

- Set a password for privileged mode.
- Create local credentials for user access.
- Configure the console port to authenticate using local credentials.

## Lab Setup
1. You will need access to a Cisco router or switch running Cisco IOS.
2. Access to the device console via a console cable.
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate these configurations

## Instructions

### Step 1: Access the device console

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202023-10-17%20at%208.32.58%20PM.png)

### Step 2: Enter Global Configuration Mode

Once you're at the device prompt, enter the global configuration mode by typing:

![](/images/Screenshot%202023-10-17%20at%208.35.18%20PM.png)

```
Router> enable
Router# configure terminal
Router(config)#
```


### Step 3: Set a password for Privileged Mode

To secure privileged EXEC mode, set a password:

![](/images/Screenshot%202023-10-17%20at%208.35.57%20PM.png)

```plaintext
Router(config)# enable secret YOUR_SECRET_PASSWORD
```

Replace `YOUR_SECRET_PASSWORD` with a strong password of your choice.

### Step 4: Create Local Credentials

1. To create a local username and password, and set the privilege level to **15**, this will allow this user to automatically login to privielge mode, use the following command:

![](/images/Screenshot%202023-10-17%20at%208.39.03%20PM.png)

```plaintext
Router(config)# username YOUR_USERNAME privielge 0-15 secret YOUR_USER_PASSWORD
```

Replace `YOUR_USERNAME` with the username of your choice and `YOUR_USER_PASSWORD` with a strong password of your choice.

### Step 5: Secure the Console Port

![](/images/Screenshot%202023-10-17%20at%208.39.53%20PM.png)

1. Navigate to the console line configuration:

```plaintext
Router(config)# line console 0
Router(config-line)#
```

2. Configure the console line to use the local credentials for authentication:

```plaintext
Router(config-line)# login local
```

3. (Optional) Set an exec timeout for security. This command will log out users after 10 minutes of inactivity:

```plaintext
Router(config-line)# exec-timeout 10 0
```

4. Exit the console line configuration:

```plaintext
Router(config-line)# exit
```

### Step 6: Save the Configuration

To ensure that your changes persist after a reboot, save the configuration:

```plaintext
Router(config)# end
Router# write memory
```

## Verification

![](/images/Screenshot%202023-10-17%20at%208.40.34%20PM.png)

1. Exit from the current session or restart the terminal session.
2. Try accessing the router or switch. It should prompt you for the local username and password.

## Verification 

1. Run a show command to verify the console port was configured correctly
2. Run a show command to see which user is currently logged in.

```plaintext
Router#show run | se line 
```

![](/images/Screenshot%202023-10-17%20at%208.42.32%20PM.png)


```plaintext
Router#show users all
```

![](/images/Screenshot%202023-10-17%20at%208.43.16%20PM.png)

## Conclusion

In this lab, you learned how to enhance the security of a Cisco IOS device by setting a privileged mode password, creating local user credentials, and configuring the console port to use local authentication. Always ensure to use strong passwords to maximize security.
