# CCNA Lab: Standard ACL Configuration on Cisco iOS

![](/images/f%20(2).png)

## Objective

In this lab, you will learn how to configure and apply a standard ACL to an interface and our VTY lines. Not shown in this guide is the configuration of static routes between routers,the configuration of SSH, the configuration of VLANs, trunks and Router-on-a-stick. By the end of this lab, you should be able to:

- Create standard ACLS
- Apply standard ACLs to an interface
- Apply standard ACLs to the VTY lines

## Lab Setup

1. You will need two Cisco routers, two Cisco switches, and four endpoints.
2. Access to all devices via console.
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Instructions

### Step 1: Security standards we need to configure


1. Users in VLAN 10 should not be able to ping both servers
2. Users in VLAN 20 should not be able to SSH to R2
3. The file server should not be able to reach the ENG VLAN 20


First lets figure where to apply the ACL. Standard ACLs should be as close to the destination as possible.

![](/images/Standard%20ACL%20(1).png)

### Step 2: Configure the first ACL for our security standards list above on R2

Security standard 1

![](/images/Screenshot%202024-12-09%20at%204.18.55%E2%80%AFPM.png)

```
enable
!
conf t
!
access-list 10 deny 10.10.10.0 0.0.0.255 log 
access-list 10 permit any log 
!
inteface Gi0/0.900
 ip access-group 10 out 
 exit
```

### Step 3: Configure the second ACL for our security standards listed above on R2

Security standard 2

![](/images/Screenshot%202024-12-09%20at%204.20.23%E2%80%AFPM.png)

```
access-list 20 deny 10.10.20.0 0.0.0.255 log 
access-list 20 permit any log 
!
line vty 0 4 
 access-class 20 in 
 exit
```

### Step 4: Configure the third ACL for our security standards listed above on R1

Security standard 3

![](/images/Screenshot%202024-12-09%20at%204.21.53%E2%80%AFPM.png)

**R1**
```
access-list 30 deny host 10.10.10.9 log 
access-list 30 permit any log 
!
interface Gi0/0.20 
 ip access-class 30 out 
 exit
```

### Step 5: Perform testing on our ACLs

For this testing we will remove the ACL, check connectivity and then apply the ACL and test again. In this guide we will just demonstrate security standard 1.

**R2**
```
conf t

!
interface Gi0/0.900
 no ip access-group 10 out
 exit
```

Now go to R1 and lets perform a ping test.

```
ping 10.10.9.20 source Gi0/0.10
```

![](/images/Screenshot%202024-12-09%20at%204.25.09%E2%80%AFPM.png)

As we can see here our VLAN 10 Users can currently ping the servers. Let's apply the ACL and see if our ping test is successful.

```
conf t
!
interface Gi0/0.900
 ip access-group 10 out
 exit
```

![](/images/Screenshot%202024-12-09%20at%204.26.04%E2%80%AFPM.png)

![](/images/Screenshot%202024-12-09%20at%204.26.40%E2%80%AFPM.png)

We can also use a show command to see matches or hits on our ACL

```
show access-lists 10
```

![](/images/Screenshot%202024-12-09%20at%204.27.09%E2%80%AFPM.png)

### Step 6: Perform the same tests on the other ACLs if you want further proof your ACLs work as intended.

### Standard ACLs in Cisco IOS: Summary & Conclusion


**Standard ACLs** primarily focus on filtering traffic based solely on the source IP address. Unlike extended ACLs, which can evaluate multiple parameters like source and destination IP addresses, transport protocol types, port numbers, etc., standard ACLs offer a simpler and more direct filtering mechanism.

### Key Features:

1. **Source IP Filtering:** Standard ACLs only evaluate the source IP address of packets to determine whether to permit or deny the traffic.

2. **Order Matters:** All ACLs, including standard ones, are processed sequentially in the order they appear. Once a match is found, no subsequent entries in the list are checked. If no conditions match, an implicit "deny all" condition is applied at the end.

3. **Granularity:** Despite being less granular than extended ACLs, standard ACLs serve scenarios where basic source-based filtering is needed. They can be used for security, routing decisions, or other traffic control applications.

### Best Practices:

**Placement:** Since standard ACLs only evaluate the source IP, it's generally recommended to place them as close to the destination as possible to avoid unnecessary traffic traversing the network.

**Descriptive Naming (For Named ACLs):** Though you can use numbered ACLs, Cisco IOS also allows you to use named ACLs, providing more descriptive context. Example:

### Conclusion:

Standard ACLs in Cisco IOS offer a straightforward way to control traffic based on source IP addresses. They're an essential tool in the network administrator's toolkit for scenarios where simple source-based filtering is sufficient. When setting up ACLs, it's crucial to plan, review, and test configurations to ensure they achieve the desired outcomes without causing unintended disruptions.