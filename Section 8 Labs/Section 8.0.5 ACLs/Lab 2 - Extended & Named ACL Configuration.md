# CCNA Lab: Extended and Named ACLs

![](/images/Screenshot%202024-12-09%20at%204.34.24%E2%80%AFPM.png)

## Objective

In this lab, you will learn how to configure and apply an extended/named ACL to your interfaces and VTY lines. Not shown in this guide is the configuration of static routes between routers,the configuration of SSH, the configuration of VLANs, trunks and Router-on-a-stick. By the end of this lab, you should be able to:

- Create and understand extended ACLs
- Create and understand the difference in the named ACL
- How to edit a named ACL
- The best practice for placing an extended ACL

## Lab Setup

1. You will need two Cisco routers, two Cisco switches, and four endpoints.
2. Access to all devices via console.
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Instructions

### Step 1: Security standards we need to configure

```
1. Users from VLAN 10 and VLAN 20 should not be able to telnet or make http connections to the web server (10.10.9.10/32)
2. Only users from the ENG VLAN 20 should have SSH access to R2
```

### Step 2: ACL Placement

First lets figure where to apply the ACL. Extended ACLs should be as close to the source as possible.

![](/images/Standard%20ACL%20(2).png)

As you can see from this screenshot we will need to only allow the ENG Subnet to SSH to R2, so we will place the ACL on our VTY lines on R2. We will also deny telnet and HTTP to our Server Farm, we will apply our ACL inbound on R1 on Gi0/0.10, and Gi0/0.20. This is the closest to the source.

### Step 3: Configure and apply the first extended ACL for our first security standard.

Configure the Extended ACL. We will first use a Named ACL. 

**R1**
```
conf t
!
ip access-list extended DENY_HTTP_TELNET 
 deny tcp 10.10.10.0 0.0.0.255 10.10.9.0 0.0.0.255 eq www
 deny tcp 10.10.20.0 0.0.0.255 10.10.9.0 0.0.0.255 eq www
 deny tcp 10.10.10.0 0.0.0.255 10.10.9.0 0.0.0.255 eq 23
 deny tcp 10.10.20.0 0.0.0.255 10.10.9.0 0.0.0.255 eq 23 
 permit tcp any any log 
 exit
!
interface Gi0/0.10  
 ip access-group DENY_HTTP_TELNET in
 exit
!
interface Gi0/0.20 
 ip access-group DENY_HTTP_TELNET in
 exit
!
```

![](/images/Screenshot%202024-12-09%20at%205.00.28%E2%80%AFPM.png)

### Step 4: Configure and apply the second extended ACL for our second security standard.

For this Extended ACL we will use a numbered ACL

**R2**
```
conf t
!
access-list 130 deny tcp 10.10.10.0 0.0.0.255 any eq 22 log
access-list 130 permit ip any any log
!
line vty 0 4
 access-class 130 in
 exit
!
```

![](/images/Screenshot%202024-12-09%20at%205.01.37%E2%80%AFPM.png)


### Step 5: Editing ACLs

Editing Names ACLs is simple. We go into the ACL named sub-configuration mode, and then we can resequence our Access Control Entries, or delete an Access Control Entry.

![](/images/Screenshot%202024-12-09%20at%205.07.17%E2%80%AFPM.png)

Now lets edit our named ACL with the following commands.

**R1**
```
conf t
!
ip access-list extended DENY_HTTP_TELNET 
 no 10 deny tcp 10.10.10.0 0.0.0.255 10.10.9.0 0.0.0.255 eq telnet log
 5 deny tcp 10.10.30.0 0.0.0.255 10.10.9.0 0.0.0.255 eq telnet log 
 exit
!
```

![](/images/Screenshot%202024-12-09%20at%205.08.02%E2%80%AFPM.png)

Editing numbered ACLs is not as simple as a named ACL. To edit a numbered ACL we must copy and paste the ACL to a text editor, make our changes there, delete the ACL, and then re-paste the ACL configuration. This will not be shown in this giude, but will be demonsrated in the corresponding videos.

## Extended ACLs in Cisco IOS: Summary & Conclusion

**Extended ACLs** in Cisco IOS offer a more granular control over network traffic compared to standard ACLs. They enable filtering based not only on source and destination IP addresses but also on protocol type, source and destination ports, and other criteria, making them versatile for various complex network scenarios.

### Key Features:

1. **Detailed Traffic Filtering**: Extended ACLs can filter on:
   - Source IP address
   - Destination IP address
   - IP protocol type (e.g., TCP, UDP, ICMP)
   - Source and destination port numbers (e.g., HTTP, SSH, Telnet)

2. **Sequential Processing**: As with standard ACLs, entries in an extended ACL are processed top-down. Once a match is found, subsequent entries are skipped. If no match is identified, an implicit "deny all" is applied at the end.

3. **Flexibility**: Their detailed criteria make extended ACLs ideal for:
   - Security policies
   - Complex traffic routing
   - Quality of Service (QoS) implementations

### Usage in Cisco IOS:

- **Configuration**: 
  To define an extended ACL, use the following syntax:

```
access-list [ACL number] {permit | deny} [protocol] [source IP] [wildcard mask] [destination IP] [wildcard mask] [optional protocol-specific criteria]
```


*Note*: The ACL number for extended ACLs typically ranges from 100 to 199 and 2000 to 2699.

- **Application**: 
Once created, extended ACLs should be applied to interfaces or various IOS features to be effective. Example:

```
interface GigabitEthernet0/1
ip access-group [ACL number] {in | out}
```


- **Best Practices**:
1. **Placement**: Given their granularity, it's often recommended to place extended ACLs as close to the source of the traffic as feasible to limit unwanted traffic early.

2. **Descriptive Naming** (For Named ACLs): While numbered ACLs are available, using named ACLs can provide clearer context. Example:
   ```
   ip access-list extended [Name]
   ```

3. **Regular Review**: Due to the detailed nature of extended ACLs, they require meticulous management and regular reviews to avoid potential oversights or misconfigurations.

### Conclusion:

Extended ACLs in Cisco IOS are powerful tools for network administrators, offering precise control over traffic flow in a network. Their versatility is unmatched when it comes to crafting detailed traffic filters. Proper configuration, regular reviews, and understanding their sequential processing are pivotal to harness their full potential and maintain a secure and efficient network environment.
