# CCNA Lab: Basic VLAN Configuration on Cisco Switch

![](/images/Screenshot%202024-01-14%20at%209.22.12%20PM.png)

## Objective 
In this lab, you will learn how to configure standard VLANs on a Cisco iOS switch. You will also learn how to delete standard VLANs from the vlan database file, which is stored on the flash. By the end of this lab, you should be able to:

- Create VLANs
- Name VLANs
- Assign VLANs to a switchport
- Delete the vlan.dat file

## Lab Setup
1. You will need a Cisco iOS switch. A physical device or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Port numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

## Instructions

### Step 1: Access the device console

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202023-10-19%20at%2012.55.11%20AM.png)

### Step 2: Enter Global Mode

Once you login or get the device prompt, enter the global configuration mode by typing and entering:

![](/images/Screenshot%202023-10-17%20at%208.35.18%20PM.png)

```
switch> enable
switch# configure terminal
switch(config)#
```


### Step 3: Configure VLAN 10 and 20

Take a look at the Network Diagram above and take note of the VLANs that need to be created. 

First we will create our VLANs and give them a name.

![](/images/Screenshot%202023-10-19%20at%208.50.11%20PM.png)

```plaintext
switch(config)#vlan 10
switch(config-vlan)#name ENGINEERING
switch(config-vlan)#vlan 20
switch(config-vlan)#name MARKETING
switch(config-vlan)#exit
switch(config)#
```

### Step 4: Assign the VLANs

Now we will need to assign the VLANs to the correct ports and ensure the interfaces are placed into `access` mode.

![](/images/Screenshot%202023-10-19%20at%208.51.47%20PM.png)

```plaintext
switch(config)#interface gi0/0
switch(config-if)#description //MARKETING VLAN\\
switch(config-if)#switchport mode access
switch(config-if)#switchport access vlan 20
switch(config-if)#exit
switch(config)#interface Gi0/1
switch(config-if)#description //ENGINEERING VLAN\\
switch(config-if)#switchport mode access
switch(config-if)#switchport access vlan 10
switch(config-if)#exit
```

### Step 5: Now we will place the rest of the interfaces into the blackhole VLAN.

First lets run a show command to see which interfaces are unused. 

![](/images/Screenshot%202023-10-19%20at%208.52.53%20PM.png)

```plaintext
switch#show interface status
```

We will need place interface Eth0/2 and Eth0/3 in a blackhole VLAN. The blackhole VLAN protects us from malicious threat actors that could plug devices into our switches. This VLAN is a VLAN that is not connected to any subnet or broadcast domain.

![](/images/Screenshot%202023-10-19%20at%208.58.49%20PM.png)

```plaintext
switch#conf t
switch(config)#interface range eth0/2 - 3
switch(config-if-range)#switchport access vlan 888
```
### Step 6: Now we will verify the VLANs created.

We will execute the following command to verify the VLANs.

![](/images/Screenshot%202023-10-19%20at%209.00.43%20PM.png)

```plaintext
switch#show vlan brief
```

### Step 7: Delete the vlan.dat file

Now we will delete the vlan.dat file which is typically stored in the flash. On this device which is emulating a Cisco switch on unix, the flash path is as follows `unix:`

![](/images/Screenshot%202023-10-19%20at%209.50.06%20PM.png)



### Conclusion/Summary

VLANs, or Virtual Local Area Networks, are a fundamental concept in networking that allows network administrators to logically segment a physical network into multiple isolated and independent networks. These virtual networks operate as if they were separate physical networks, even though they share the same physical infrastructure. Here's a brief summary of what VLANs are and why they are used:

**What are VLANs?**
- VLANs are a method of dividing a single physical network into multiple logical networks.
- They are created by grouping network devices (such as computers, switches, and routers) into virtual LANs based on criteria like department, function, or security requirements.
- Devices within the same VLAN can communicate with each other as if they were on the same physical network, while communication between devices in different VLANs requires routing.

**Why Use VLANs?**
1. **Network Segmentation:** VLANs help segregate network traffic, enhancing security and reducing broadcast traffic. This is particularly important in large networks.

2. **Improved Performance:** By isolating traffic, VLANs can improve network performance. Broadcast storms in one VLAN won't affect others.

3. **Security:** VLANs enhance network security by isolating sensitive or critical data from other parts of the network. Access control and firewall policies can be applied at the VLAN level.

4. **Resource Optimization:** VLANs allow for better resource utilization. You can allocate network resources based on department or function, ensuring that critical applications have sufficient bandwidth.

5. **Simplified Management:** VLANs simplify network management by logically organizing devices. Changes to network configurations can be made at the VLAN level, reducing complexity.

6. **Guest Networks:** VLANs are often used to create guest networks. Guests can access the internet but are isolated from internal resources.

7. **Compliance:** VLANs can help organizations comply with regulatory requirements by segmenting data and controlling access to sensitive information.

In summary, VLANs provide a flexible and efficient way to manage and secure a network by dividing it into logical segments. They are an essential tool for network administrators to optimize performance, enhance security, and simplify network management in modern IT environments.
