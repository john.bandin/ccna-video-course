# CCNA Lab: Advanced VLAN Configuration on a Cisco switch

![VLAN Lab 2](/images/Screenshot%202024-01-14%20at%209.21.11%20PM.png)

## Objective

In this lab you will configure all the standard access VLANs and Voice VLANs. We will also test if we have IP reachability across the different VLANs. By the end of this lab, you should be able to:

- Configure access and voice VLANs
- Assign VLANs to the correct ports
- Perform a wireshark capture on EVE-NG

## Lab Setup
1. You will need access to two Cisco Switches running Cisco iOS
2. Access to the devices via a console port or remote connection
3. End devices to test IP reachability
4. For our CCNA course we are emulating all resources using EVE-NG

## Disclaimer

Port numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

## Instructions

### Step 1: Turn on devices and access the CLI

Turn on all your devices and using a console cable and terminal software (e.g., PuTTY, Tera Term) connect to your devices. For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202023-10-19%20at%2010.05.19%20PM.png)

### Step 2: Configure VLANs on both switches

From global configuration mode on both switches configure and assign all VLANs

![SW1 VLAN Creation](/images/Screenshot%202023-10-19%20at%2010.09.49%20PM.png)

![SW2 VLAN Creation](/images/Screenshot%202023-10-19%20at%2010.14.12%20PM.png)

```
en
!
conf t
!
vlan 10
 name SALES
 vlan 20
 name FINANCE
 vlan 30
 name MARKETING
 vlan 40
 name VOICE
 exit
!
```

Now let's assign all the VLANs correctly, we will start with `SW1`

![Assiging VLANs on SW1](/images/Screenshot%202023-10-19%20at%2010.17.16%20PM.png)

```
!
interface eth0/0
 switchport mode access 
 switchport access vlan 10
 exit
!
interface eth0/1
 switchport mode access
 switchport access vlan 20
 exit
!
interface eth0/2
 switchport mode access
 switchport voice vlan 40
 exit
!
```

Now let's assign the VLANs to the correct ports on `SW2`.

![](/images/Screenshot%202023-10-19%20at%2010.21.55%20PM.png)

```
!
interface eth0/0
 switchport mode access
 switchport access vlan 10
 exit
!
interface eth0/1
 switchport mode access
 switchport access vlan 20
 exit
!
interface eth0/2
 switchport mode access
 switchport access vlan 30
 exit
!
```

### Step 3. Verify the VLAN assignment

We will verify our VLANs are assigned correctly using the following command on both switches.

```
show vlan brief
```

![](/images/Screenshot%202023-10-19%20at%2010.24.09%20PM.png)

![](/images/Screenshot%202023-10-19%20at%2010.26.28%20PM.png)

### Step 4. Test IP reachability

Now we will console into our PCs and do a ping test. Refer to the network diagram above for the correct IP addresses. This test is meant to prove that VLANs logically segment broadcast domains, and without a Layer 3 Routing device with the ability to send traffic between subnets our users on different VLANs will not be able to ping each other.

![](/images/Screenshot%202023-10-19%20at%2010.32.54%20PM.png)

As you can see we do not have IP reachability from our user in `VLAN 20` and our user in `VLAN 10`. 

### Step 5. Wireshark Capture

Now lets send a continuous ping from our PC in `VLAN 10` on SW1 to our PC in `VLAN 20` and perform a packet capture on the interface between switches.

![](/images/Screenshot%202023-10-19%20at%2010.36.16%20PM.png)

To perform a packet capture using wireshark on your EVE-NG switches, `right-click` on the switch, click `capture` and then choose the correct interface. In our case it is `Eth0/3`

![](/images/Screenshot%202023-10-19%20at%2010.36.02%20PM.png)

From this capture we will inspect our Pings and notice how the ping from our PC in `VLAN 10` are encapsulated with the VLAN 10 tag.

![](/images/Screenshot%202023-10-19%20at%2010.41.03%20PM.png)

### Conclusion/summary

When using VLANs in a network, a Layer 3 routing device, such as a router or a Layer 3 switch, is essential to facilitate the routing of traffic between the VLANs. Here's a summary of why a Layer 3 routing device is necessary in a VLAN-based network:

1. **VLAN Isolation:** VLANs are designed to create isolated, logically segmented networks within a single physical network infrastructure. Devices within the same VLAN can communicate with each other seamlessly, but by default, they cannot communicate with devices in other VLANs.

2. **Inter-VLAN Communication:** To enable communication between devices in different VLANs, a Layer 3 routing device is required. This device has the capability to understand and process Layer 3 (IP) information, allowing it to route traffic between VLANs based on IP addresses.

3. **Subnet Separation:** Each VLAN is typically associated with a specific IP subnet. Without routing capabilities, devices in separate VLANs would be unable to communicate because they reside in different IP subnets.

4. **Security and Control:** Having a Layer 3 routing device at the core of VLAN intercommunication provides granular control over traffic flows between VLANs. Network administrators can implement access control lists (ACLs) and firewall rules to restrict or permit specific types of traffic between VLANs, enhancing security.

5. **Quality of Service (QoS):** A Layer 3 routing device can also manage Quality of Service (QoS) policies to prioritize or throttle certain types of traffic between VLANs, ensuring that critical applications receive the necessary bandwidth.

6. **Routing Between Physical and Virtual Networks:** In virtualized environments, a Layer 3 routing device can also route traffic between virtual LANs (vLANs) and physical networks, facilitating seamless communication between physical and virtual resources.

7. **Flexibility and Scalability:** As a network grows and evolves, the addition of new VLANs or changes to existing VLAN configurations may be required. A Layer 3 routing device offers the flexibility to adapt to these changes by creating, modifying, or removing VLANs as needed.

8. **Efficient Traffic Handling:** Routing devices are typically designed to handle routing tasks efficiently, optimizing the flow of traffic between VLANs. This efficiency contributes to overall network performance.

In summary, a Layer 3 routing device plays a critical role in enabling inter-VLAN communication within a network that utilizes VLANs. It ensures that devices in different VLANs can communicate while providing control, security, and flexibility in managing traffic between these logical network segments.