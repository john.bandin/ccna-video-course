# CCNA Lab: CDP and LLDP Configuration on Cisco iOS

![](/images/Screenshot%202024-01-14%20at%2010.03.25%20PM.png)

## Objective 
In this lab, you will learn how to configure and enable CDP and LLDP on Cisco iOS. You will learn how to enable and disable both discovery protocols globally and at the interface level.

- enable/disable CDP Locally/Globally
- enable/disable LLDP Locally/globally
- Set CDP hello and hold timers


## Lab Setup
1. You will need a Cisco iOS switch and a Cisco iOS Router. A physical device or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Port numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

## Instructions

### Step 1: Configure the trunk port

First we will set the trunk port between SW2 and SW3 as a static trunk. Configure these commands on both switches.

![](/images/Screenshot%202024-01-14%20at%2010.10.04%20PM.png)

```
# interface gi0/1
# switchport trunk encapsulation dot1q
# switchport mode trunk
```

### Step 2: Set IP addresses

Now we will set IP addresses on our Network Devices. We are doing this so we can see the IP addresses in the CDP/LLDP information. For the router we will configure the physical interface, for our switches we will set the management IP address by creating an SVI, we will create `interface vlan 1`. The default management VLAN.

![](/images/Screenshot%202024-01-14%20at%2010.12.59%20PM.png)

![](/images/Screenshot%202024-01-14%20at%2010.13.29%20PM.png)

```
SW2
!
# interface vlan 1
# ip address 192.168.0.2 255.255.255.0
# no shut

SW3
!
# interface vlan 1
# ip address 192.168.0.3 255.255.255.0
# no shut
```

Now we will configure the routers IP address.

![](/images/Screenshot%202024-01-14%20at%2010.15.29%20PM.png)

```
# interface gi0/0
# ip address 192.168.0.1 255.255.255.0
# no shut
```

### Step 3: Configure CDP Globally

To configure CDP globally run the following command from **global** configuration mode. Run this command on all the network devices.

![](/images/Screenshot%202024-01-14%20at%2010.17.10%20PM.png)

```
# cdp run
```

### Step 4: Configure LLDP Globally

To configure LLDP globally run the following command from **global** configuration mode. Run this command on all the network devices.

![](/images/Screenshot%202024-01-14%20at%2010.18.11%20PM.png)

```
# lldp run
```

### Step 5: Disable CDP/LLDP on the interface

Now, to meet security requirements we will turn off all discovery protocols on interfaces connected to user ports. In our Lab environment these are ports `Gi0/2` and `Gi0/3` on SW3.

![](/images/Screenshot%202024-01-14%20at%2010.22.17%20PM.png)

```
# interface range gi0/2 - 3
# no lldp receive
# no lldp transmit
# no cdp enable
```
### Step 6: Set the hello and hold timers for CDP 

Now we will change the hello and hold timers for CDP on SW2. We are running this command to demonstrate the different features you can configure for your CDP and LLDP messages. We will set the send interval to 60 seconds using the `cdp timer` command. We will set the hold/dead timers to 180 using the `cdp holdtime` command.

![](/images/Screenshot%202024-01-14%20at%2010.25.24%20PM.png)

```
# cdp timer 60
# cdp holdtime 180
```

### Step 7: Verify the CDP and LLDP neighbors

Now we will run several different commands from SW2 and verify that CDP and LLDP messages are being sent and received. We will also take a look at the important information we can gain from our `show` commands.

```
# show cdp neighbors
```

![](/images/Screenshot%202024-01-15%20at%201.11.03%20PM.png)

The `show cdp neighbors` command will show you the local interface and the distant interface of the connected Cisco Device, as well as the hostname. 

```
# show lldp neighbors
```

![](/images/Screenshot%202024-01-15%20at%201.20.39%20PM.png)

The `show cdp neighbors` command will show you the local interface and the distant interface of the connected Cisco Device, as well as the hostname. 

```
show cdp neighbors [X/X] detail
```

![](/images/Screenshot%202024-01-15%20at%201.22.53%20PM.png)

The `show cdp neighbor detail` command will show you a lot more output and informationn about your neighbor. Some additional information you can find out with this command is the Management IP address which you can then use to SSH into that neighbor device, and also the platform version you are connected too. 

```
show lldp neighbor [X/X] detail
```

![](/images/Screenshot%202024-01-15%20at%201.32.47%20PM.png)


The `show lldp neighbor detail` command will show you a lot more output and informationn about your neighbor. Some additional information you can find out with this command is the Management IP address which you can then use to SSH into that neighbor device, and also the platform version you are connected too. 

## Step 8: Verify with Wireshark

Now lets take a wireshark capture from SW2 --> SW3 on Gi0/1. Below will be highlighted information from both CDP and LLDP messages that are apart of the TLVs.ds

![](/images/Screenshot%202024-01-15%20at%201.53.57%20PM.png)

## Summary:

## CDP (Cisco Discovery Protocol):

### Purpose: 

CDP is a Cisco proprietary protocol used to discover information about directly connected Cisco devices.

### Functionality: 

It provides details about the device type, the version of the operating system running, native VLAN, and IP address. This information is crucial for network topology mapping and troubleshooting.

### Operation: 

CDP operates at the Data Link Layer (Layer 2) and is enabled by default on most Cisco devices.

### Usage: 

It is especially useful in environments dominated by Cisco equipment.

## LLDP (Link Layer Discovery Protocol):

### Purpose: 

LLDP is a standardized network discovery protocol used across various vendors' devices.

### Functionality: 

Like CDP, LLDP provides device identification, capabilities, and the identity of neighboring devices on a local broadcast domain.

### Operation: 

LLDP operates at Layer 2 and offers a similar range of information as CDP but with the added advantage of being vendor-neutral.

### Usage: 

It is ideal for multi-vendor network environments.

## Conclusion:

In your CCNA course, understanding CDP and LLDP is crucial for network management and troubleshooting. While CDP is specific to Cisco devices and offers detailed information about them, LLDP provides a vendor-neutral solution, essential for interoperability in diverse network environments. Both protocols play a significant role in network discovery, helping administrators map network topologies, identify connected devices, and understand their functionalities. As a network professional, being proficient in both CDP and LLDP will enhance your ability to manage, troubleshoot, and optimize network infrastructures effectively.