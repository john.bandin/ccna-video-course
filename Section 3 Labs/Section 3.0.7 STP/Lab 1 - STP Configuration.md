# CCNA Lab: Spanning-Tree Configuration Configuration on Cisco Switch

![](/images/Screenshot%202024-01-17%20at%202.30.14%20PM.png)

## Objective 
In this lab, you will learn how to change the Spanning-Tree modes on a Cisco switch, determine the root port, and change the STP priority per VLAN.

- Set STP Priority
- Change STP Modes
- Determine the root port

## Lab Setup
1. You will need a four Cisco iOS switch. A physical device or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: Create VLANs and Trunks

First thing we'll need to do is create the VLANs and Trunks on all the switches. Assigning the users to the correct VLANs is not a necessary step for these lab objectives. So we will just configure trunks and create the VLANs locally on each switch.


**DSW1**

![](/images/Screenshot%202024-01-17%20at%208.14.39%20AM.png)

```
!
vlan 10,20
 exit
!
interface range gi0/0 - 3
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
!
```

**DSW2**

![](/images/Screenshot%202024-01-17%20at%208.14.39%20AM.png)

```
!
vlan 10,20
 exit
!
interface range gi0/0 - 3
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
!
```

**ACCSW3**

![](/images/Screenshot%202024-01-17%20at%208.18.16%20AM.png)

```
!
vlan 10,20
 exit
!
interface range gi0/0 - 1
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
``` 

### Step 3: Set the Root Switch for VLAN 10 and 20

We're going to set DSW1 as the root switch for VLAN 10. We're going to set DSW2 as the root switch for VLAN 20. And then we are going to verify the root ports on the access switches.

**DSW1**

For the first distro switch we are going to set priority using a numeric value for VLAN 10. Use the following commands below.

![](/images/Screenshot%202024-01-17%20at%201.37.38%20PM.png)

```
!
spanning-tree vlan 10 priority 4096
```

**DSW2**

Now for Distro SW2 we are going to use a `macro` to set the root for VLAN 20 on DSW2.

![](/images/Screenshot%202024-01-17%20at%201.40.04%20PM.png)

```
!
spanning-tree vlan 20 root primary
```

### Step 4: Verify the root ports 

Now we will go on our Access Switches and verify the root

![](/images/Screenshot%202024-01-17%20at%201.56.33%20PM.png)

![](/images/Screenshot%202024-01-17%20at%201.57.03%20PM.png)

Using the show `show spanning-tree` commands we can see from ACCSW3 that the root ports are different for both VLANs. For VLAN 10 the root port is Gi0/2 which is directly connected to DSW1, and for VLAN 20 the root port is Gi0/3 which is directlyt connected to DSW2.

### Step 5: Configure user ports to come up immediately

Now we will configure our user facing ports to not send BPDUs so they do not have to participate in the spanning-tree election, and will come up/up immediately. Use this command on both ACCSW3 and ACCSW4 user facing interfaces.

![](/images/Screenshot%202024-01-17%20at%202.01.02%20PM.png)

```
interface range gi1/0 - 1
 spanning-tree portfast
```

### Summary

Spanning Tree Protocol (STP) in a Cisco environment is an essential network protocol that ensures a loop-free topology for Ethernet networks. The primary function of STP is to prevent broadcast storms, which can arise from redundant links in a switched network. By identifying and putting redundant links into a blocking state, STP allows only one active path between two network devices, eliminating the possibility of looping. This stabilization of the network structure is vital for maintaining the efficiency and reliability of a network. In a Cisco switched architecture, STP plays a critical role by automatically discovering and managing redundant paths, ensuring that data packets do not get trapped in infinite loops and that network resources are used optimally. As such, STP is a cornerstone in maintaining the resilience and performance of a Cisco switched network, safeguarding it against broadcast storms and the associated potential for network failures and downtime.