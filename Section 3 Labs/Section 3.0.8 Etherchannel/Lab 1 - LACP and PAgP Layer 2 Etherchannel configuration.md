# CCNA Lab: LACP and PAgP Layer 2 Etherchannel Configuration on Cisco Switch

![](/images/Screenshot%202024-01-17%20at%204.37.40%20PM.png)

## Objective 
In this lab, you will learn how to configure LACP and PAgP etherchannels. You will also learn how to verify the etherchannels are configured correctly.

- Configure LACP
- Configure PAgP
- Verify the Etherchannel is bundled

## Lab Setup
1. You will need three Cisco iOS switches. Physical devices or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: Create VLANs

First thing we'll need to do is create the VLANs on all the switches. Assigning the users to the correct VLANs is not a necessary step for these lab objectives. So we will just configure trunks and create the VLANs locally on each switch. Run the following commands on all switches.

```
!
vlan 10,20
 exit
```

### Step 3: Configure LACP SW1 --> SW2

Now we will configure our first etherchannel. The first etherchannel we will create we will use the negotiation protocol LACP from SW1 ---> SW2.

- SW1 will be actively sending the negotiations 
- SW2 will only be receiving negotiations messages.

Follow these exact steps and commands below to correctly configure the Etherchannel. 


**SW1**

![](/images/Screenshot%202024-01-17%20at%203.28.56%20PM.png)

```
!
default interface range gi0/0 - 1
!
interface range gi0/0 - 1
 shut 
 channel-group 10 mode active
```

![](/images/Screenshot%202024-01-17%20at%203.31.56%20PM.png)

```
!
interface po 10
 switchport trunk encapsulation dot1q
 switchport mode trunk
```

In these steps we want to completely reset our interfaces using the `default interface` command. Then before we configure the channel-grouup we will shut the interfaces. Take note that this method is the cleanest way to bring up an etherchannel but does not work in some cases, like remotely configuring etherchannels were you do not have console access to the switch.

**SW2**

![](/images/Screenshot%202024-01-17%20at%203.33.50%20PM.png)

```
!
default interface range gi0/0 - 1
!
interface range gi0/0 - 1
 shut
 channel-group 10 mode passive
```

![](/images/Screenshot%202024-01-17%20at%203.34.52%20PM.png)

```
!
int po 10
 switchport trunk encapsulation dot1q
 switchport mode trunk
```

Now that we have both sides configured, let's turn our member interfaces back on using the `no shut` command. Execute this command on both switches.

![](/images/Screenshot%202024-01-17%20at%203.37.06%20PM.png)

```
!
interface range gi0/0 - 1
 no shut
 exit

```

Now we will verify if the port-channels came up and are passing traffic using the `show etherchannel summary` command.

![](/images/Screenshot%202024-01-17%20at%203.39.04%20PM.png)

As you can see from the image above, our Etherchannel/portchannel is bundled and passing traffic. The Captial `SU` means we are passing traffic as a Layer 2 etherchannel. And the lowercase `p`indicates that our member interfaces are bundled and operational in our Etherchannel.

### Step 3: Configure LACP SW2 --> SW3

Now we will configure the etherchannel between SW2 ---> SW3 using LACP. 

- SW2 will be actively negotiating
- SW3 will also be actively neogotiating

**SW2**

![](/images/Screenshot%202024-01-17%20at%204.22.45%20PM.png)

```
!
default interface range gi1/0 - 1
!
interface range gi1/0 - 1
 shut
 channel-group 5 mode active
 exit
!
interface port-channel 5
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
!
interface range gi1/0 - 1
 no shut
 exit
!
do wr
```

**SW3**

![](/images/Screenshot%202024-01-17%20at%204.22.45%20PM.png)

```
!
default interface range gi1/0 - 1
!
interface range gi1/0 - 1
 shut
 channel-group 5 mode active
 exit
!
interface port-channel 5
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
!
interface range gi1/0 - 1
 no shut
 exit
!
do wr
```

Now we will use the `show etherchannel su` command to verify the etherchannel is bundled and in use.

![](/images/Screenshot%202024-01-17%20at%204.24.53%20PM.png)

```
show etherchannel summary
```

As you can see from the image above, our Etherchannel/portchannel is bundled and passing traffic. The Captial `SU` means we are passing traffic as a Layer 2 etherchannel. And the lowercase `p`indicates that our member interfaces are bundled and operational in our Etherchannel.

### Step 4: Configure PAgP SW1 ---> SW3

Now we will configure the Cisco proprietary Etherchannel protocol PAgP. We will configure this etherchannel from SW1 ---> SW3

- SW1 will be actively sending negotiations to SW3
- SW3 will only be receiving negotiations from SW1

**SW1**

![](/images/Screenshot%202024-01-17%20at%204.31.11%20PM.png)

``` 
!
default interface range gi0/2 - 3
!
interface range gi0/2 - 3
 shut
 channel-group 15 mode desirable
 exit
!
interface port-channel 15
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
!
interface range gi0/2 - 3
 no shut
 exit
!
do wr
```

**SW3**

![](/images/Screenshot%202024-01-17%20at%204.31.23%20PM.png)

``` 
!
default interface range gi0/2 - 3
!
interface range gi0/2 - 3
 shut
 channel-group 15 mode auto
 exit
!
interface port-channel 15
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
!
interface range gi0/2 - 3
 no shut
 exit
!
do wr
```

Now we will verify the etherchannel/port-channel is bundled, passing traffic and "in-use". We will use the `show etherchannel summary` command to verify. You can run this command at either switch.

![](/images/Screenshot%202024-01-17%20at%204.33.08%20PM.png)

```
show etherchannel summary
```

As you can see from the image above, our Etherchannel/portchannel is bundled and passing traffic. The Captial `SU` means we are passing traffic as a Layer 2 etherchannel. And the lowercase `p`indicates that our member interfaces are bundled and operational in our Etherchannel.

## Summary/Conclusion
EtherChannel technology in Cisco networks allows multiple physical Ethernet links to be bundled into a single logical link, offering increased bandwidth, load balancing, and redundancy. Two primary EtherChannel protocols are Link Aggregation Control Protocol (LACP) and Port Aggregation Protocol (PaGP). Both serve similar purposes but with different operational approaches.

1. **LACP (Link Aggregation Control Protocol):**
   - **Use and Functionality:** LACP is part of the IEEE 802.3ad standard and is used for automatic bundling of links. It facilitates the bundling by exchanging LACP packets between Ethernet ports to automatically configure and maintain the link aggregation.
   - **Benefits:**
     - **Flexibility and Control:** LACP supports cross-vendor interoperability, providing flexibility in network designs.
     - **Dynamic Configuration:** It can dynamically bundle and de-bundle links, automatically adapting to network configuration changes.
     - **Load Balancing:** Ensures efficient utilization of member links in the bundle, distributing traffic across all operational links.

2. **PaGP (Port Aggregation Protocol):**
   - **Use and Functionality:** PaGP is Cisco's proprietary protocol and serves a similar purpose as LACP. It facilitates the automatic creation of EtherChannel links by negotiating EtherChannel formation and managing member ports' inclusion and exclusion.
   - **Benefits:**
     - **Cisco Integration:** As a Cisco proprietary protocol, PaGP is often well-integrated into Cisco environments, offering smooth operation with Cisco devices.
     - **Load Balancing:** Like LACP, it allows for load balancing over the member links, though the options for load distribution might differ.
     - **Simplicity:** For Cisco-centric networks, PaGP can be a straightforward choice without the need for cross-vendor protocol considerations.

**Why We Use EtherChannel Technologies like LACP and PaGP:**

1. **Increased Bandwidth:** By aggregating multiple links, EtherChannel significantly increases the bandwidth between switches or between switches and servers, improving the overall data transfer rate.

2. **Redundancy and High Availability:** If one of the bundled links fails, traffic is automatically redirected to the remaining links without any network disruption, ensuring continuous availability.

3. **Load Balancing:** EtherChannel evenly distributes traffic across all active links, optimizing the utilization of available bandwidth and preventing any single link from becoming a bottleneck.

4. **Network Simplification:** Logical representation of multiple physical links as a single EtherChannel link simplifies network configuration and management.

In summary, EtherChannel technologies like LACP and PaGP are integral in optimizing network performance, reliability, and efficiency. They provide a means to scale bandwidth, enhance fault tolerance, and manage link aggregation in a streamlined manner, making them essential in modern network infrastructures.