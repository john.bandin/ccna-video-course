# CCNA Lab: Advanced Trunking Configuration on a Cisco switch

![Trunking Lab 2](/images/Screenshot%202024-01-23%20at%201.50.39%20PM.png)

## Objective

In this lab you will configure a static trunk so there can be inter-VLAN communication. By the end of this lab, you should be able to:

- Configure a static trunk
- Configure a trunk using DTP
- Configure trunk pruning

## Lab Setup 

1. You will need access to three Cisco Switches running Cisco iOS
2. Access to the devices via a console port or remote connection
3. End devices to test IP reachability
4. For our CCNA course we are emulating all resources using EVE-NG

## Disclaimer

Port numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

## Instructions

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)


### Step 2: Enter global mode

Once you login or get the device prompt, enter the global configuration mode on all three switches by typing and entering:

![](/images/Screenshot%202024-01-14%20at%209.34.14%20PM.png)

```
switch>enable
switch#configure terminal
switch(config)#
```
### Step 3: Configure VLANs and Assign Access ports on all 3 switches

Take a look at your network diagram and determine which VLANs need to be created on all switches. Use the commands below to create all necessary VLANs. Do these commands on both your switches

**SW1**

![](/images/Screenshot%202024-01-16%20at%201.55.01%20PM.png)

```
# vlan 10,20
# exit
```

**SW2**

![](/images/Screenshot%202024-01-16%20at%201.56.45%20PM.png)

```
!
vlan 10,20
 exit
!
interface gi0/2
 switchport mode access
 switchport access vlan 20
!
interface gi0/3
 switchport mode access
 switchport access vlan 10
!
```

**SW3**

![](/images/Screenshot%202024-01-16%20at%202.02.26%20PM.png)

```
!
vlan 10,20
 exit
!
interface gi0/1
 switchport mode access
 switchport access vlan 10
!
interface gi0/3
 switchport mode access
 switchport access vlan 20
!
```

### Step 4: Configure a static trunk port

Now we will configure the trunk between SW2 and SW3. We will create a static trunk and not use the Dynamic Trunking Protocol to form a trunk. We will set the switchport mode ourselves. And ensure that negotation is also turned off. Do these commands for both

**SW2**

![](/images/Screenshot%202024-01-16%20at%202.11.42%20PM.png)

```
interface gi0/0
!
switchport trunk encapsulation dot1q
switchport mode trunk
switchport nonegotiate
```

**SW3**

![](/images/Screenshot%202024-01-16%20at%202.15.31%20PM.png)

```
interface gi0/0
!
switchport trunk encapsulation dot1q
switchport mode trunk
switchport nonegotiate
```

### Step 6: Configure trunks using DTP

Now we will configure trunking between SW2 ---> SW1 and SW3 ---> SW1 using DTP. Follow these procedures listed below for configuration.
- SW1 will actively being sending negotiations to both SW3 and SW2
- SW3 and SW2 should only be receving negotiations messages
- Ensure that only VLAN 10, and 20 are allowed on the trunk between SW1 ---> SW3

Follow these commands below.

**SW1**

![](/images/Screenshot%202024-01-16%20at%206.10.06%20PM.png) 

```
!
interface range gi0/1 - 2
 switchport trunk encapsulation dot1q
 switchport mode dynamic desirable
```

**SW2**

![](/images/Screenshot%202024-01-16%20at%206.12.33%20PM.png)


```
!
interface gi0/1
 switchport trunk encapsulation dot1q
 switchport mode dynamic auto
```


**SW3**

![](/images/Screenshot%202024-01-16%20at%206.15.03%20PM.png)

```
!
interface gi0/2
 switchport trunk encapsulation dot1q
 switchport mode dynamic auto
```

### Step 7: Verify the trunks

Now we want to verify the trunks and take note of how the trunks were formed using the `show interface trunk` command. We will also use the `show interfaces [X/X] switchport` command to ensure that negotiation is off on our static trunk from SW2 ---> SW3.

**SW1**

![](/images/Screenshot%202024-01-16%20at%206.19.21%20PM.png)

```
!
show interfaces trunk
```

- Take note on the `encapsulation` and `mode` columns on the output. These trunks were formed using desirable mode locally and are using `dot1q` encapsulation.

**SW2**

![](/images/Screenshot%202024-01-16%20at%206.21.20%20PM.png)

```
!
show interfaces trunk
```

- Take note on the different ways the static trunk vs. the dynamic trunk were formed. From SW2 ---> SW1, locally SW2 was in `dynamic auto` mode only receiving DTP messages.
- The static trunk you can see the mode is `on`

**SW3**

![](/images/Screenshot%202024-01-16%20at%206.26.59%20PM.png)

```
show interfaces gi0/0 switchport
```

- Take note of how on the actual interface negotiation is turned off. 

### Step 8: Configure/verify trunk pruning

Now we will configure trunk pruning from SW2 ---> SW3. Use the command below on SW2 to ensure that only VLAN 1,10, and 20 can be sent across the trunk.

![](/images/Screenshot%202024-01-16%20at%206.42.49%20PM.png)

```
!
interface gi0/0
 switchport trunk allowed vlan 1,10,20
```

Now lets verify those VLANs are pruned using the `show interfaces trunk` command.

![](/images/Screenshot%202024-01-16%20at%206.43.53%20PM.png)

```
!
show interfaces trunk
```

## Summary/Conclusion

### Basic Concept:

A trunk port is a type of port on a switch used to carry traffic for multiple VLANs. Unlike an access port, which is associated with a single VLAN, a trunk port can transport traffic for several VLANs simultaneously. This is crucial for inter-VLAN routing and for the management of large-scale networks.

### VLAN Tagging:

Trunk ports utilize a tagging mechanism known as IEEE 802.1Q to distinguish between traffic from different VLANs. When a frame enters a trunk port, a VLAN tag is added to the frame to identify its VLAN. This tag is removed before the frame exits the trunk port on the other side.

### Use in Network Topology:

Trunk ports are predominantly used to connect switches to each other or to connect switches to routers in a network. This allows VLANs to span across multiple switches, enabling devices in the same VLAN but connected to different switches to communicate as if they were on the same physical network.

### Configuration:

On Cisco switches, trunk ports are configured using specific IOS commands. Administrators can define which VLANs are allowed on the trunk port and set the native VLAN (the VLAN whose traffic is not tagged).

### Advantages:

Trunking provides flexibility and efficiency in network design. It reduces the number of physical links needed between switches, as one trunk port can carry the traffic of multiple VLANs.

It simplifies network configuration and management, especially in large networks with numerous VLANs.

### Security Considerations:

Proper configuration of trunk ports is vital for network security. Misconfigured trunk ports can lead to VLAN hopping attacks where an attacker can gain access to traffic across different VLANs.
It's essential to restrict the VLANs that can pass through a trunk port and to ensure that the native VLAN is different from the user VLANs.

### Practical Application in CCNA Labs:

In a CCNA lab, practicing the configuration and management of trunk ports is crucial. You might be tasked with setting up trunk links between switches, configuring allowed VLAN lists, and understanding how data flows across these trunk links.
Simulating network traffic and analyzing how VLAN tags are added and removed in trunk ports can be an enlightening exercise for understanding how larger networks function.
