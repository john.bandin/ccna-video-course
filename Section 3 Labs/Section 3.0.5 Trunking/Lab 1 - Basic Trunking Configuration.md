# CCNA Lab: Basic Trunking Configuration on a Cisco switch

![Trunking Lab 1](/images/Trunking%20Lab%201.png)

## Objective

In this lab you will configure a static trunk so there can be inter-VLAN communication. By the end of this lab, you should be able to:

- Configure a static trunk

## Lab Setup 

1. You will need access to two Cisco Switches running Cisco iOS
2. Access to the devices via a console port or remote connection
3. End devices to test IP reachability
4. For our CCNA course we are emulating all resources using EVE-NG

## Disclaimer

Port numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

## Instructions

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: Enter global mode

Once you login or get the device prompt, enter the global configuration mode by typing and entering:

![](/images/Screenshot%202024-01-14%20at%209.34.14%20PM.png)

```
switch>enable
switch#configure terminal
switch(config)#
```
### Step 3: Configure VLAN 10

Take a look at your network diagram and determine which VLANs need to be created. Use the commands below to create all necessary VLANs. Do these commands on both your switches

![](/images/Screenshot%202024-01-14%20at%209.36.17%20PM.png)

```
switch(config)#vlan 10
switch(config-vlan)#name SALES
switch(config-vlan)#exit
```

### Step 4: Assign the VLANs to the appropraite ports

Take a look at your network diagram and determine which ports need to be assigned to VLAN 10.

![](/images/Screenshot%202024-01-14%20at%209.38.57%20PM.png)

```
switch(config)#interface gigabitethernet 0/1
switch(config-if)#switchport mode access
switch(config-if)#switchport access vlan 10
```

### Step 5: Configure a static trunk port

Now we will configure the trunk between SW1 and SW2. We will create a static trunk and not use the Dynamic Trunking Protocol to form a trunk. We will set the switchport mode ourselves. Do these commands on both switches.

![](/images/Screenshot%202024-01-14%20at%209.42.49%20PM.png)

### Step 6: Verify the trunk

Now we will verify the trunk port is up by executing the "show interface trunk" command from privilege mode. Then we will verify what mode the connected switchports are operating in, using the "show interfaces [X/X] switchport" command. 

![](/images/Screenshot%202024-01-14%20at%209.44.57%20PM.png)

```
# show interface trunk
```

Now lets execute the **switchport** command.

![](/images/Screenshot%202024-01-14%20at%209.46.13%20PM.png)

```
#show interfaces gi0/0 switchport
```

## Summary/Conclusion

### Basic Concept:

A trunk port is a type of port on a switch used to carry traffic for multiple VLANs. Unlike an access port, which is associated with a single VLAN, a trunk port can transport traffic for several VLANs simultaneously. This is crucial for inter-VLAN routing and for the management of large-scale networks.

### VLAN Tagging:

Trunk ports utilize a tagging mechanism known as IEEE 802.1Q to distinguish between traffic from different VLANs. When a frame enters a trunk port, a VLAN tag is added to the frame to identify its VLAN. This tag is removed before the frame exits the trunk port on the other side.

### Use in Network Topology:

Trunk ports are predominantly used to connect switches to each other or to connect switches to routers in a network. This allows VLANs to span across multiple switches, enabling devices in the same VLAN but connected to different switches to communicate as if they were on the same physical network.

### Configuration:

On Cisco switches, trunk ports are configured using specific IOS commands. Administrators can define which VLANs are allowed on the trunk port and set the native VLAN (the VLAN whose traffic is not tagged).

### Advantages:

Trunking provides flexibility and efficiency in network design. It reduces the number of physical links needed between switches, as one trunk port can carry the traffic of multiple VLANs.

It simplifies network configuration and management, especially in large networks with numerous VLANs.

### Security Considerations:

Proper configuration of trunk ports is vital for network security. Misconfigured trunk ports can lead to VLAN hopping attacks where an attacker can gain access to traffic across different VLANs.
It's essential to restrict the VLANs that can pass through a trunk port and to ensure that the native VLAN is different from the user VLANs.

### Practical Application in CCNA Labs:

In a CCNA lab, practicing the configuration and management of trunk ports is crucial. You might be tasked with setting up trunk links between switches, configuring allowed VLAN lists, and understanding how data flows across these trunk links.
Simulating network traffic and analyzing how VLAN tags are added and removed in trunk ports can be an enlightening exercise for understanding how larger networks function.
