# CCNA Lab: Switched Virtual Interface Configuration on Cisco iOS

![](/images/Screenshot%202024-01-18%20at%202.27.51%20PM.png)

## Objective 
In this lab, you will configure Switched Virtual Interfaces on a Distribution switch that will be acting as the routing devices for all the users and network infrastructure.

- Configure SVIs
- Configure a Management IP address on access switches

## Lab Setup
1. You will need a three Cisco iOS switches. These can be Physical devices or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: Configure the LAN

At this point in your studies you should already know how to configure VLANs, Trunks and set IP addresses on the Virtual PCs in our EVE-NG environment. Below will be the baseline configurations for each switch, and also the IPs for the PCs.

**SW1**

```
!
hostname SW1
!
vlan 10,20,30
 exit
!
interface range gi0/0 - 1
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport nonegotiate
 exit
!
do wr
!
```

**SW2**

```
!
hostname SW2
!
vlan 10,20,30
 exit
!
interface range gi0/0, gi0/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport no
 exit
!
interface gi1/0
 switchport mode access
 switchport access vlan 20
 spanning-tree portfast
 exit
!
interface gi1/1
 switchport mode access
 switchport access vlan 10
 spanning-tree portfast
 exit
!
do wr
!
```

**SW3**

```
!
hostname SW3
!
vlan 10,20,30
 exit
!
interface range gi0/1 - 2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport no
 exit
!
interface gi1/0
 switchport mode access
 switchport access vlan 20
 spanning-tree portfast
 exit
!
interface gi1/1
 switchport mode access
 switchport access vlan 10
 spanning-tree portfast
 exit
!
do wr
```

**PC8 - VLAN20**

```
ip 192.168.20.2/24 192.168.20.1

save
```

**PC5 VLAN20**

```
ip 192.168.20.3/24 192.168.20.1

save
```

**PC6 - VLAN10**

```
ip 192.168.10.3/24 192.168.10.1

save
```

**PC7 VLAN10**

```
ip 192.168.10.2/24 192.168.10.1

save
```

### Step 3: Configure DSW1 as the "Router"

First thing we will need to do is configure `IP routing` on the Distro switch. Then we will create all the necessary SVIs'

- Configure IP routing on DSW1
- Configure all necessary SVIs

![](/images/Screenshot%202024-01-18%20at%2011.47.44%20AM.png)

```
ip routing
!
interface vlan 1
 shut
 exit
!
interface vlan 10
 description //VLAN 10 DG\\
 ip address 192.168.10.1 255.255.255.0
 no shut
 exit
!
interface vlan 20
 description //VLAN 20 DG\\
 ip address 192.168.20.1 255.255.255.0
 no shut
 exit
!
interface vlan 30
 description //VLAN 30 DG\\
 ip address 192.168.30.1 255.255.255.0
 no shut
 exit
!
do wr
!
```

### Step 4: Configure Management Interfaces

Now for all our other network infrastructure (SW2 & SW3) we will create the Management SVI, under the VLAN 30 subnet. This is our dedicated management subnet.

**SW2**
```
interface vlan 1
 shut
 exit
!
interface vlan 30
 ip address 192.168.30.2 255.255.255.0
 no shut
 exit
!
ip default-gateway 192.168.30.1
!
do wr
!
```

**SW3**
```
interface vlan 1
 shut
 exit
!
interface vlan 30
 ip address 192.168.30.3 255.255.255.0
 no shut
 exit
!
ip default-gateway 192.168.30.1
!
do wr
!
```

### Step 5: Test reachability between users

Now we will test reachability between our users in separate VLANs. From PC6(VLAN 10) we will ping PC8 in VLAN 20. Refer to the image below to test reachability.

![](/images/Screenshot%202024-01-18%20at%2011.51.33%20AM.png)

