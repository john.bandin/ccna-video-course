# CCNA Lab: Router-on-a-stick Configuration on Cisco iOS

![](/images/Screenshot%202024-01-18%20at%2010.22.05%20AM.png)

### Objective 
In this lab, you will configure Router-on-a-stick on a Cisco iOS router, and then verify that users can ping each other. Additionally you will configure a Management IP address on each Access switch so that they can be managed remotely.

- Configure sub-interfaces
- Configure a Management IP address on access switches
- 

## Lab Setup
1. You will need a three Cisco iOS switches and a Cisco iOS router. These can be Physical devices or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: Configure the LAN.

At this point in your studies you should already know how to configure VLANs, Trunks and set IP addresses on the Virtual PCs in our EVE-NG environment. Below will be the baseline configurations for each switch, and also the IPs for the PCs.

**SW1**

```
!
hostname SW1
!
vlan 10,20,30
 exit
!
interface range gi0/0 - 2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport nonegotiate
 exit
!
do wr
```

**SW2**

```
!
hostname SW2
!
vlan 10,20,30
 exit
!
interface range gi0/0 - 1
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport no
 exit
!
interface gi1/0
 switchport mode access
 switchport access vlan 10
 spanning-tree portfast
 exit
!
interface gi1/1
 switchport mode access
 switchport access vlan 20
 spanning-tree portfast
 exit
!
do wr
```

**SW3**

```
!
hostname SW3
!
vlan 10,20,30
 exit
!
interface range gi0/0, gi0/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 switchport no
 exit
!
interface gi0/1
 switchport mode access
 switchport access vlan 10
 spanning-tree portfast
 exit
!
interface gi1/1
 switchport mode access
 switchport access vlan 20
 spanning-tree portfast
 exit
!
do wr
```

**PC8 - VLAN20**

```
ip 192.168.20.3/24 192.168.20.1

save
```

**PC5 VLAN10**

```
ip 192.168.10.3/24 192.168.10.1

save
```

**PC6 - VLAN20**

```
ip 192.168.20.2/24 192.168.20.1

save
```

**PC7 VLAN10**

```
ip 192.168.10.2/24 192.168.10.1

save
```

### Step 3: Configure Sub-interfaces

Now we will jump into our router and configure our sub-interfaces. A couple important things to note.

- Each sub-network/VLAN will need its own sub-interface
- The physical link will still need to be turned on `no shut`
- The sub-interface number has no correlation to the VLAN tag. 

Follow these commands below to configure all the necessary sub-interfaces.

![](/images/Screenshot%202024-01-18%20at%2011.09.53%20AM.png)

```
!
interface gi0/0
 description //ROAS PHYSICAL LINK\\
 no shut
 exit
!
interface gi0/0.10
 description //VLAN 10 LINK\\
 encapsulation dot1q 10
 ip address 192.168.10.1 255.255.255.0
 exit
!
interface gi0/0.20
 description //VLAN 20 LINK\\
 encapsulation dot1q 20
 ip address 192.168.20.1 255.255.255.0
 exit
!
interface gi0/0.30
 description //VLAN 30 LINK\\
 encapsulation dot1q 30
 ip address 192.168.30.1 255.255.255.0
 exit
!
do wr
```

### Step 4: Test Inter-VLAN communication

Now lets hop on one of our PCs on VLAN 10, and attempt to ping a PC on VLAN 20. We will do this in two steps.

- First lets hop on PC8 and ping our default gateway `192.168.20.1`
- Next we will attempt to ping PC7 that is a part of VLAN 10 hanging off SW3

Connect to PC8 and follow this photo below to verify reachability.

![](/images/Screenshot%202024-01-18%20at%2011.13.56%20AM.png)

As you can see we now have Inter-VLAN communication and full reachability.

### Step 5: Set Switches MGMT IP addresses

Now we will need to ensure our switches are reachable via Layer 3. We will do this by configuring a `switched virtual interface` on our switches. We have set aside an entire subnet for managing our network infrastructure - `192.168.30.0/24`. This is the IP address space we will use when we use remote protocols like SSH. Follow these commands below.

**SW1**

![](/images/Screenshot%202024-01-18%20at%2011.17.41%20AM.png)

```
interface vlan 1
 shut
 exit
!
interface vlan 30
 ip address 192.168.30.2 255.255.255.0
 no shut
 exit
!
ip default-gateway 192.168.30.1
!
do wr
```

**SW2**

```
interface vlan 1
 shut
 exit
!
interface vlan 30
 ip address 192.168.30.3 255.255.255.0
 no shut
 exit
!
ip default-gateway 192.168.30.1
!
do wr
```

**SW3**

```
interface vlan 1
 shut
 exit
!
interface vlan 30
 ip address 192.168.30.4 255.255.255.0
 no shut
 exit
!
ip default-gateway 192.168.30.1
!
do wr
```
### Step 5: Verify the SVIs

Now we will run our `show ip int br` command to verify the SVI was created, and we will also do a `ping` test to ensure our SW1 has reachability to the default gateway. We also run a `show cdp neighbors detail` from SW1 to ensure we see the Mangement IPs from our neighbor switches: SW2 & SW3

![](/images/Screenshot%202024-01-18%20at%2011.19.55%20AM.png)

```
show ip int br | in Vlan30
```

Next lets do a ping test from privilege mode on SW1.

![](/images/Screenshot%202024-01-18%20at%2011.20.47%20AM.png)

```
ping 192.168.30.1
```

And for our final verification step we will run a cdp command.

![](/images/Screenshot%202024-01-18%20at%2011.23.50%20AM.png)

```
show cdp neighbor details
```

# Router on a Stick: An In-Depth Summary

"Router on a stick," also known as "one-armed router," is a specialized networking setup used for managing traffic between different VLANs (Virtual Local Area Networks) on a network. This configuration employs a single physical interface on a router to route traffic between multiple VLANs, presenting an efficient and cost-effective solution for small to medium-sized network environments.

## Concept of VLANs
- **VLANs (Virtual Local Area Networks):** VLANs segment a physical network into multiple logical networks. Each VLAN forms its own broadcast domain. Devices in the same VLAN communicate directly, but communication between different VLANs requires routing.

## Basics of Router on a Stick
- **Single Physical Interface:** Utilizes a single physical interface on the router to manage traffic for multiple VLANs, configured as a trunk link.
- **Trunk Link:** Capable of carrying traffic for multiple VLANs simultaneously, distinguishing traffic using VLAN tagging protocols like IEEE 802.1Q.

## Configuration Elements
- **Subinterfaces:** The physical interface on the router is divided into subinterfaces, each representing a VLAN.
- **Encapsulation and Tagging:** Each subinterface is configured with VLAN tagging (e.g., IEEE 802.1Q) to identify its VLAN traffic.
- **IP Addressing:** Subinterfaces are assigned IP addresses to serve as the default gateway for their respective VLANs.

## Operational Flow
- **Ingress Traffic:** Incoming packets are checked for their VLAN tag to determine the relevant subinterface.
- **Routing Decision:** The router routes the packet based on its routing table. If the destination is in a different VLAN, the packet is routed to the respective subinterface.
- **Egress Traffic:** The packet leaves the router through the same physical interface but tagged for the destination VLAN.

## Advantages
- **Cost-Effective:** Reduces the need for multiple routers or switches with routing capabilities.
- **Simplified Infrastructure:** Minimizes the number of physical devices and interfaces, easing management and troubleshooting.
- **Flexibility:** Easy to add or remove VLANs by configuring the router, without needing additional hardware.

## Considerations
- **Throughput Limitation:** The single physical interface can become a bottleneck if inter-VLAN traffic volume is high.
- **Reliability:** Presents a single point of failure. If the router or interface fails, all inter-VLAN routing is affected.

In conclusion, the router on a stick configuration is an effective approach for managing inter-VLAN routing in a streamlined and cost-effective manner. It's particularly beneficial for small to medium-sized networks, but larger or more critical network setups should consider throughput and reliability factors.
