# CCNA Lab: NTP Configuration on Cisco iOS

![](/images/Screenshot%202024-12-09%20at%203.25.09%E2%80%AFPM.png)

## Objective 
In this lab, you will configure a clock on R1 and then configure R2 to be the NTP Client. After configuration we will verify the NTP configuration. For a bonus we will configure NTP authentication as well.

## Lab Setup

1. You will need two Cisco iOS Routers. These can be Physical devices or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: Configure a Baseline on R1 and R2

On R1 and R2 we will need to configure a basic baseline on both routers. Use the **TEMPLATE** below.

```
enable
!
conf t
!
hostname XX
!
line con 0
 logging synchronous
 exit
!
interface Gi0/1
 ip address 192.168.10.X 255.255.255.0
 no shut
 exit
!
```

### Step 3: Set the clock on R1

In this network R1 will be the NTP Server/Master and R2 will be the NTP Client. Since this network has no connection to the internet we will statically set R1's clock, and then have R2 use NTP to synchronize to R1. First we will set R1's clock, then configure it as an NTP server with a stratum level of 1.

**NOTE** Setting the clock is done from privilege mode, NOT global configuration mode.

**R1**
```
clock set hh:mm:ss DAY MONTH YEAR
!
conf t
!
ntp master 1
```

**R2**
```
conf t
!
ntp server 192.168.10.1
```

Now that we have statically set the clock on R1 and configured R2 to be an NTP client, let's verify they're synchronization.

### Step 4: Verfiy that the Routers are synched

To verify that NTP is synched we will use the `show ntp associations` command from the NTP client

**R2**
```
show ntp associations
```

![](/images/Screenshot%202024-12-09%20at%203.35.24%E2%80%AFPM.png)

## Conclusion

We have configured the clock on R1, and also configured R1 to be an NTP Server/Master. R2 was configured as an NTP client, synchronizing to R1

## Summary

# Understanding Network Time Protocol (NTP)

## What is NTP?

The **Network Time Protocol (NTP)** is a networking protocol designed to synchronize the clocks of devices across a network. By ensuring that all systems operate on the same time, NTP facilitates accurate timestamps for logging, security protocols, and coordinated network operations.

NTP uses **Coordinated Universal Time (UTC)** as a reference and synchronizes device clocks within milliseconds of the UTC standard. It achieves this through hierarchical time sources, where servers are organized in strata, ranging from Stratum 0 (highly precise time sources like atomic clocks) to Stratum 15.

---

## Importance of NTP in Networks

Accurate time synchronization provided by NTP is critical for several reasons:

1. **Event Correlation**  
   Network devices, servers, and applications rely on accurate timestamps for log files. This is essential for correlating events across multiple systems, especially when troubleshooting or investigating security incidents.

2. **Authentication and Security**  
   Time-sensitive protocols like **Kerberos** and certificate-based systems depend on synchronized clocks to validate authentication tokens and ensure secure communication.

3. **Efficient Operations**  
   Network coordination tasks, such as routing updates, failover mechanisms, and scheduled jobs, rely on accurate time to ensure efficient operation and prevent conflicts.

4. **Compliance and Auditing**  
   Many regulatory standards require accurate and auditable time records, particularly in financial and healthcare industries.

---

## Practical Use in CCNA Labs

In CCNA labs, configuring NTP ensures that routers, switches, and other devices have synchronized time. This is a common requirement for lab scenarios involving:

- **Syslog and SNMP Configuration**: Accurate timestamps are necessary for log and monitoring data.
- **Network Troubleshooting**: Correlating logs across multiple devices becomes easier with synchronized clocks.
- **Security Protocols**: Testing features like IPSec or SSH may involve time-sensitive operations where NTP is crucial.

---

### Example NTP Configuration in a Lab

```bash
# Set the NTP server on a Cisco device
ntp server 192.168.1.1

# Verify NTP associations
show ntp associations

# Verify clock synchronization
show clock
```
