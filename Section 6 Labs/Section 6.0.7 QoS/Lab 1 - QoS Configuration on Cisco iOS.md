# QoS  Lab

![](/images/qos%20lab.png)

1. Turn on all devices
2. Paste baseline from GITLAB
3. Verify all connectivity and IP Addressing
4. Classify the HTTP traffic and mark it with a AF43 bit.
    4a. Set the the bandwidth to 20% and place in a priority Queue (LLQ)
5. Classify SSH traffic and mark it with a AF12 bit.
    5a. Set the bandwidth to 20% and place in a WFQ
6. Classify youtube traffic and mark it with a AF32 bit.
    6a. Set the bandwidth to 40% and place in a priority Queue (LLQ)
7. Classify ICMP traffic and mark it with a CS2 bit
    7a. Set the bandwidth to 10% and place in a priority queue (LLQ)
8. Classify VoIP traffic and mark it with a EF bit.
    8a. Set the bandwidth to 5% and place in a priorit queue (LLQ)
9. Send traffic using each protocol that you classified (youtube wont be possible unless you install an actual windows/linux VM)
10. Run show commands to see if the policy is being used. What do you see?
11. Now actually apply the policy on the interface outbound towards the internet.
12. Run a wireshark capture to see if the markings are in the IPv4 header.

