# FTP Lab

![](/images/ftp%20lab.png)

1. Turn on all devices
2. Paste baseline from GITLAB
3. Verify all connectivity and IP Addressing
4. Configure a FTP Server that is reachable within your network (For this lab it is my local computer that is bridged to the EVE-NG topology)
5. Set the global FTP username and password on your router.
6. Copy your iOS from R1 to a folder on your FTP Server.
7. Perform a wireshark capture of the FTP connection.