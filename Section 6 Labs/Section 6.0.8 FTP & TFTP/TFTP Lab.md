# TFTP Lab

![](/images/tftp%20lab.png)

1. Turn on all devices
2. Paste baseline from GITLAB
3. Verify all connectivity and IP Addressing
4. Configure a TFTP Server that is reachable within your network (For this lab it is my local computer that is bridged to the EVE-NG topology)
5. Copy your iOS from R1 to a folder on your TFTP Server.
6. Perform a wireshark capture of the TFTP connection.