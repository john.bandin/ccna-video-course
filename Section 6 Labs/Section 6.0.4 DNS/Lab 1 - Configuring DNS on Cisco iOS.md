# DNS Lab

![](/images/dns%20lab.png)

1. Turn on all devices
2. Paste baseline from GITLAB
3. Verify all connectivity and IP Addressing
4. Configure R1 and SW1 with a full domain name.
5. Configure R1 to send/resolve DNS requests to 8.8.8.8 and 8.8.4.4
6. Ensure that domain lookups are enabled globally for SW1 and R1
7. Ensure that you can SSH to SW1 from R1 using only the FQDN of SW1. 
8. Verfiy all the DNS configuration on R1
9. Perform a wireshark capture from R1 to the internet during a DNS request from PC1 trying to connect to yahoo.com