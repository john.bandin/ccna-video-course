# DHCP Relay Lab

![](/images/DHCP%20Relay%20lab.png)

1. Turn on all devices
2. Paste baseline from GITLAB
3. Verify all VLANs, Trunks and IP Addressing
4. Configure a DHCP relay for each network.
5. Verify that the DHCP relay is working on R1
6. Perform a wireshark capture between R1 and the DHCP server during a DHCP request from a client.