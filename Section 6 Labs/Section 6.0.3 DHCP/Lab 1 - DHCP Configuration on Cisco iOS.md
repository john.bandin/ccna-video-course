# CCNA Lab: DHCP Configuration on Cisco iOS

![](/images/Screenshot%202025-01-16%20at%2010.08.35%E2%80%AFAM.png)

## Objective 
In this lab, you will configure a three DHCP pools on a Cisco iOS Router. This Router will serve as the DHCP server for your campus network.

## Lab Setup

1. You will need two Cisco iOS Routers. These can be Physical devices or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: Configure a Baseline on R1 and the Switches.

```
R1
!
enable
!
conf t
!
hostname R1
!
line con 0
 logging synchronous
 exit
!
interface Gi0/0
 no shut
 exit
!
interface gi0/0.10
 encapsulation dot1q 10
 ip address 192.168.10.1 255.255.255.0
 exit
!
interface gi0/0.20
 encapsulation dot1q 20
 ip address 192.168.20.1 255.255.255.0
 exit
!
interface gi0/0.30
 encapsulation dot1q 30
 ip address 192.168.30.1 255.255.255.0
 exit
!
```

```
SW12
!
enable
!
conf t
!
hostname SW12
!
line con 0
 logging synchronous
 exit
!
interface range gi0/0 - 2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
!
vlan 10,20,30
 exit
!
```

```
SW2
!
enable
!
conf t
!
hostname SW2
!
line con 0
 logging synchronous
 exit
!
interface range gi0/0 - 1
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
!
vlan 10,20,30
 exit
!
Int gi0/2
 switchport mode access
 switchport access vlan 10
 switchport voice vlan 30
 exit
!
int gi0/3
 switchport mode access
 switchport access vlan 10
 exit
!
int gi1/0
 switchport mode access
 switchport access vlan 20
 exit
!
```

```
SW3
!
enable
!
conf t
!
hostname SW3
!
line con 0
 logging synchronous
 exit
!
interface range gi0/0, gi0/2
 switchport trunk encapsulation dot1q
 switchport mode trunk
 exit
!
vlan 10,20,30
 exit
!
Int gi0/1
 switchport mode access
 switchport access vlan 10
 switchport voice vlan 30
 exit
!
int gi1/0
 switchport mode access
 switchport access vlan 10
 exit
!
int gi0/3
 switchport mode access
 switchport access vlan 20
 exit
!
```

### Step 3: Set the DHCP Pools on the Cisco iOS Device

In this step we will setup a DHCP pool for all our VLANs and users. For the marketing and sales VLAN we will have the following configuration.
- default gateway
- network address
- dns server
- lease time

For the Voice VLAN we will add an additional option to our DHCP pool, which is the Option 150 address. This will point phones to their Call Manager server to download their profile. We will be selecting a random IP address to simulate this.

```
SALES DHCP POOL
!
conf t
!
ip dhcp pool SALES
 network 192.168.10.0 /24
 default-router 192.168.10.1
 lease 7
 dns-server 8.8.8.8 8.8.4.4
 exit
!
```

```
MARKETING DHCP POOL
!
conf t
!
ip dhcp pool MARKETING
 network 192.168.20.0 /24
 default-router 192.168.20.1
 lease 7
 dns-server 8.8.8.8 9.9.9.9
 exit
!
```

```
VOICE DHCP POOL
!
conf t
!
ip dhcp pool VOICE
 network 192.168.30.0 /24
 default-router 192.168.30.1
 lease 7
 dns-server 8.8.8.8 9.9.9.9
 option 150 ip 45.2.2.3 23.1.1.1
 exit
!
```

### Step 4: Exclude IP addresses

Now we will practice excluding a range of IP addresses for static use. We sometimes have to set static IP addresses for devices in our network. Some examples are Printers, VTC, File Shares (NAS). When we exlcude IP addresses in Cisco iOS it is a range of IPs. 

```
EXCLUDE IP ADDRESSES IN SALES VLAN

!
conf t
!
ip dhcp excluded-address 192.168.10.50 192.168.10.60
```

### Step 5: Verify DHCP pool

Now we will console into our PCs and set them to DHCP to test our DHCP configuration. We will do this for a PC in the SALES VLAN and a PC in the MARKETING VLAN.

VPC9

![](/images/Screenshot%202025-01-16%20at%2010.25.03%E2%80%AFAM.png)

VPC8

![](/images/Screenshot%202025-01-16%20at%2010.26.00%E2%80%AFAM.png)

Here we can see our DHCP Configuration was successful because the PCs have gotten their IP addressing automatically.

### Step 6: Verify the DHCP binding

Now we will run some show commands that verify our DHCP pools bindings.

```
show ip dhcp pool
```

![](/images/Screenshot%202025-01-16%20at%2012.36.05%E2%80%AFPM.png)

```
show ip dhcp binding
```

![](/images/Screenshot%202025-01-16%20at%2012.36.41%E2%80%AFPM.png)


## Summary/Conclusion

In this lab, you explored the basics of DHCP operations, which simplify IP address management in networks. The key steps in DHCP include:

- DHCP Discovery: The client broadcasts a request for network configuration.
- DHCP Offer: The server responds with an available IP address and configuration options.
- DHCP Request: The client requests the offered IP configuration.
- DHCP Acknowledgment (ACK): The server confirms the lease, completing the process.

This automated process reduces administrative overhead, ensures efficient use of IP addresses, and enhances scalability in network environments. By configuring DHCP on Cisco devices, you enabled dynamic address allocation, which is crucial for modern, flexible networks.