# Static NAT Lab

![](/images/static%20nat%20lab%201.png)

1. Turn on all devices
2. Paste baseline from GITLAB
3. Verify all IP addressing and routing
4. Configure a static NAT translation for user PC1 so it can reach the web server at 8.8.8.8
5. Once NAT is configured perform a continous ping from PC1 to 8.8.8.8
6. Perform a wireshark packet capture on R1's inside and outside address.
7. Verify the NAT translations on R1.
8. Try to create another static NAT translations for PC2.