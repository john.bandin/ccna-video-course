# NAT Overload Lab

![](/images/NAT%20Overload%20Lab.png)

1. Turn on all devices
2. Paste baseline from GITLAB
3. Verify all IP addressing and routing
4. Configure PAT on R1 so that all networks in the diagram can reach the internet.
5. Verify that users in the 10.10.20.0/24 network can reach 8.8.8.8
6. Verify the NAT translation table on R1
7. Perform a wireshark capture on Gi0/1 from R1 ----> MGMT CLOUD during a continuous ping to 8.8.8.8 from host VPC4