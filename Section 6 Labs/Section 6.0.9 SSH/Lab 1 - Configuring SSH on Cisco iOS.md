# CCNA Lab: SSH Configuration on Cisco iOS

![](/images/Screenshot%202025-01-16%20at%2012.41.23%E2%80%AFPM.png)

## Objective 
In this lab, you will configure SSH on a Cisco Router and Switch. Once configured you will SSH from the router to the switch via the console connection.

## Lab Setup

1. You will need one Cisco iOS Router and one Cisco iOS switch. These can be Physical devices or an emulated switch on GNS3 or EVE-NG.
2. Access to the device via a console cable or remote protocol. (Telnet, SSH)
3. For our CCNA course by Trepa Technologies we will be using EVE-NG to simulate/emulate Cisco devices.

## Disclaimer

Interface numbers may differ in your lab environment. (I.E. Your ports may be Gigabitethernt vs. Ethernet)

### Step 1: Access the device console.

Connect to your Cisco device using a console cable and terminal software (e.g., PuTTY, Tera Term). For EVE-NG we will just click on the device to telnet in, again simulating a "console" connection.

![](/images/Screenshot%202024-01-14%20at%209.33.22%20PM.png)

### Step 2: Configure a Baseline on R1 and the switch.

```
R1

enable
!
conf t
!
hostname R1
!
line con 0 
 logging synch
 exit
!
interface gi0/0
 ip address 10.10.10.0 255.255.255.254
 no shut
 exit
!
```

```
SW1

enable
!
conf t
!
hostname SW1
!
line con 0 
 logging synch
 exit
!
interface vlan 1
 ip address 10.10.10.1 255.255.255.254
 no shut
 exit
!
```

### Step 3: Configure SSH

Now we will configure SSH on both the router and the switch. To configure SSH you need generate the asymmetric RSA keys. These public/private key pairs will be used for the SSH connections. You will also need to allow SSH as a remote protocol to be used on your VTY lines. Before you generate RSA keys you need to configure the following three things.

- Domain name
- local credentials
- hostname

Follow the commands below to configure these three items and to generate the RSA key. **NOTE** the hostname was already set in the baseline configuration above.

```
R1
!
enable
!
conf t
!
ip domain-name ccna.lab
!
username cisco privilege 15 secret cisco
!
crypto key generate rsa modulus 2048
!
```

```
SW1
!
enable
!
conf t
!
ip domain-name ccna.lab
!
username cisco privilege 15 secret cisco
!
crypto key generate rsa modulus 2048
!
```

Next we will ensure the VTY lines will allow remote connections on our devices and that they will also accept the local credentials.

``` 
R1

enable
!
conf t
!
line vty 0 4
 login local
 transport input ssh
 exit
!
```

``` 
SW1

enable
!
conf t
!
line vty 0 15
 login local
 transport input ssh
 exit
!
```

### Step 4: Verify that SSH is working

Now we will verify that SSH is working by using SSH to remote into our switch from our router. First lets ensure the router can ping the switch. 

```
R1

ping 10.10.10.1
```

![](/images/Screenshot%202025-01-16%20at%201.20.16%E2%80%AFPM.png)

Now lets SSH from R1 to the Switch.

```
ssh -l cisco 10.10.10.1
```

![](/images/Screenshot%202025-01-16%20at%201.21.03%E2%80%AFPM.png)

