# SNMP Lab

![](/images/snmp%20lab.png)

1. Turn on all devices
2. Paste baseline from GITLAB
3. Verify all connectivity and IP Addressing
4. configure R1 to be managed using SNMPv3 with authPriv as the protection for SNMP communication
5. Configure SW1 to be managed using SNMPv2c.
6. Perform a wireshark capture on the interface from the Switch-->Net MGMT Cloud
7. Notice the differences between SNMPv2 and SNMPv3
8. Perform some basic show commands to see the configuration of SNMP on both network devices.