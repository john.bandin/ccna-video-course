# SNMP Lab

![](/images/syslog%20lab.png)

1. Turn on all devices
2. Paste baseline from GITLAB
3. Verify all connectivity and IP Addressing
4. Configure/enable logging on R1
5. Configure the syslog messages to get sent to your syslog server.
6. Configure all syslog messages to have an accurate date/time stamp.
7. Ensure that all severity levels are sent to the syslog server
8. Verfiy that logging is enabled and perform a wireshark capture.